<?php

namespace App\Http\Controllers;

use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function send($type = '', Request $request)
    {
        define('MESSAGE_TYPE_CLUB', 'club');
        define('MESSAGE_TYPE_CONTACT', 'contact');
        define('MESSAGE_TYPE_REGISTER', 'register');
        define('MESSAGE_TYPE_RESERVE', 'reserve');

        $brandPrimaryColor = '#006466';
        $brandSecondaryColor = '#b69f66';
        $toAddress = 'contacto@laslomashabitat.mx'; // Email to address
        $toName = 'Las Lomas Habitat'; // Email contact name
        $emailSubject = 'Las Lomas Habitat: '; // Email Subject
        $description = '';

        switch($type){
            case MESSAGE_TYPE_CLUB: 
                $subject = 'Contacto Socio Club'; 
                $description = 'Se ha enviado la siguiente información desde el formulario de contacto:';
                break;
            case MESSAGE_TYPE_REGISTER: 
                $subject = 'Consulta de propiedad'; 
                $description = 'Se accedió a los detalles de la siguiente propiedad:';
                break;
            case MESSAGE_TYPE_RESERVE: 
                $subject = 'Reservación de propiedad'; 
                $description = 'Solicita la reserva de la siguiente propiedad:';
                break;
            
            case MESSAGE_TYPE_CONTACT: 
            default: 
                $subject = 'Contacto'; 
                $description = 'Se ha enviado la siguiente información desde el formulario de contacto:';
                break;
        }
        $emailSubject .= $subject;

        // mensaje
        $message = '
            <center>
            <table bgcolor="#ffffff" cellspacing="0" style="width: 75%;border-spacing:0;border:1px solid #e2e2e2;font-family:\'Helvetica\',sans-serif">
            <tbody>
                <tr>
                    <td bgcolor="'.$brandPrimaryColor.'" style="padding:15px 15px;color:#fff;border-bottom:4px solid '.$brandSecondaryColor.';">'.$toName.'</td>
                    <td bgcolor="'.$brandPrimaryColor.'" align="right" style="padding:15px 25px;border-bottom:4px solid '.$brandSecondaryColor.';"><br></td>
                </tr>
                <tr>
                    <td colspan="2" bgcolor="#333" style="padding:15px 15px;">
                    <h1 style="color:#fff;padding:0px;margin:0;">'.$subject.'</h1>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="justify" style="color:#333;font-size:14px;padding:30px;">
                        '.$description.'
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="left" style="padding:0 30px 30px 30px;">';
                    if(strlen($request->get('manzana','')))
                        $message .= '
                        Manzana: <strong><font color="#333">'.($request->get('manzana','')).'</font></strong><br>';
                    if(strlen($request->get('lote','')))
                        $message .= '
                        Lote: <strong><font color="#333">'.($request->get('lote','')).'</font></strong><br>';
                    $message .= '
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="left" style="padding:0 30px 30px 30px;">
                        <h3>Información del usuario</h3>';
                    if(strlen($request->get('email','')))
                        $message .= '
                        Email: <strong><font color="#333">'.($request->get('email','')).'</font></strong><br><br />';
                    if(strlen($request->get('name','')))
                        $message .= '
                        Nombre: <strong><font color="#333">'.($request->get('name','')).' '.($request->get('lastname','')).'</font></strong><br>';
                    if(strlen($request->get('phone','')))
                        $message .= '
                        '.('Teléfono').': <strong><font color="#333">'.($request->get('phone','')).'</font></strong><br>';
                    if(strlen($request->get('message','')))
                        $message .= '
                        Comentario: <strong><font color="#333">'.($request->get('message','')).'</font></strong><br>';
                    $message .= '
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center" style="padding:0 30px 30px 30px;"><font color="#aaa" style="font-size:11px;">'.("Este mensaje de correo electrónico es confidencial y/o puede contener información privilegiada. Si usted no es su destinatario o no es alguna persona autorizada por este para recibir sus correos electrónicos, NO deberá usted utilizar, copiar, revelar, o tomar ninguna acción basada en este correo electrónico o cualquier otra información incluida en el.").'</font>
                    <br>
                    </td>
                </tr>
            </tbody></table>
            </center>
        ';
        if($request->get('preview','false') === 'true'){
            echo $message;
            exit;
        }

        // Enviarlo
        $mail = new PHPMailer(true);

        try {

            if($type === MESSAGE_TYPE_CONTACT || $type === MESSAGE_TYPE_CLUB){
                // Recaptcha
                $claveSecreta = '6Lc_sj0fAAAAAEwhQ1iaHHkvC2PCrBWl-MSvoF0T';
                $token = $request->get("g-recaptcha-response",'');
                # La API en donde verificamos el token
                $url = "https://www.google.com/recaptcha/api/siteverify";
                # Los datos que enviamos a Google
                $datos = [
                    "secret" => $claveSecreta,
                    "response" => $token,
                ];
                // Crear opciones de la petición HTTP
                $opciones = array(
                    "http" => array(
                        "header" => "Content-type: application/x-www-form-urlencoded\r\n",
                        "method" => "POST",
                        "content" => http_build_query($datos), # Agregar el contenido definido antes
                    ),
                );
                $contexto = stream_context_create($opciones);
                $resultado = file_get_contents($url, false, $contexto);
                if ($resultado === false) {
                    abort(403);
                }
                $resultado = json_decode($resultado);
                $test = $resultado->success;
                if(!$test) {
                    abort(400);
                }
            }

            $mail->isSMTP();
            $mail->CharSet = 'utf-8';
            $mail->SMTPAuth =true;
            $mail->SMTPSecure = env('MAIL_ENCRYPTION');
            $mail->Host = env('MAIL_HOST'); //gmail has host > smtp.gmail.com
            $mail->Port = env('MAIL_PORT'); //gmail has port > 587 . without double quotes
            $mail->Username = env('MAIL_USERNAME'); //your username. actually your email
            $mail->Password = env('MAIL_PASSWORD'); // your password. your mail password
            $mail->addReplyTo($request->get('email',''));
            $mail->setFrom(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
            $mail->Subject = $subject;
            $mail->MsgHTML($message);
            $mail->addAddress($toAddress, $toName);
            $mail->send();
        } catch (Exception $e) {
            abort(400);
        }
    }
}
