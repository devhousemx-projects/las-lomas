<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AvailabilityModuleController extends Controller
{
    /**
     * Display the module.
     *
     * @return View
     */
    public function index(Request $request)
    {
        $data = array();
        $data['request'] = $request;
        $data['isSalesModule'] = true;
        return view('availabilityModule', $data);
    }

    public function generateTiles()
    {
        $source = 'images/simulator/img-globalArea.jpg';
        //echo $source; exit;
        $destination = public_path('images/simulator/tiles/');

        // Setup the Zoomify library.
        $zoomify = new \DanielKm\Zoomify\Zoomify();

        // Process a source file and save tiles in a destination folder.
        $result = $zoomify->process($source, $destination);
        echo 'Done!';
    }

    public function privacy(Request $request)
    {
        $data['request'] = $request;
        return view('privacy',$data);
    }
}