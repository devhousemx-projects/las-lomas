<?php

namespace App\Http\Controllers;

use App\Helpers\Feed;
use Illuminate\Http\Request;
use Response;

class FacebookController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function feed(Request $request)
    {
        $feed = new Feed;
        $data['feed'] = $feed or null;
        $data['feedPosts'] = !empty($feed) ? array_slice($feed->getFeed(3,false,$request->get('force','false')=='true' ? true : false),0,3) : null;
        return response()->json([
            'profile' => $data['feed']->details,
            'posts' => $data['feedPosts']
        ]);
    }
}
