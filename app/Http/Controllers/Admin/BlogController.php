<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Article;

class BlogController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        $data['articles'] = Article::orderBy('created_at')->get();
        return view('admin.blog.index',$data);
    }

    /**
     * Display form to create article
     */
    public function create()
    {
        return view('admin.blog.edit');
    }

    /**
     * Store article
     */
    public function store(Request $request)
    {
        $article = new Article;
        $validator = \Validator::make(
            $request->all(),
            [
                'status'    => 'required|numeric',
            ],
            [
                'required'  => 'Campo requerido.'
            ]
        );

        if ($validator->fails()) {
            return $validator->messages();
        } else {
            $response = [];
            try {
                $article->title = $request->get('title','');
                $article->slug = $request->get('slug','');
                $article->excerpt = $request->get('excerpt','');
                $article->content = addslashes($request->get('content',''));
                $article->status = $request->get('status','');
                $article->created_at = $request->get('created_at','');

                # Guardamos para obtener el ID
                $article->save();
                
                # Guardamos archivos
                    $destinationPath = public_path().'/uploads/articles/';
                    // thumb
                    if($request->hasFile('file-thumb')){
                        $thumb = $request->file('file-thumb');
                        $filename = $article->id.'-thumb-'.time().'.'.$request->file('file-thumb')->extension();
                        $thumb->move($destinationPath, $filename);
                        $article->image = $filename;
                    }
                    // head cover
                    if($request->hasFile('file-cover')){
                        $cover = $request->file('file-cover');
                        $filename = $article->id.'-cover-'.time().'.'.$request->file('file-cover')->extension();
                        $cover->move($destinationPath, $filename);
                        $article->imageHead = $filename;
                    }

                # Guardamos nuevamente con las imagenes
                $article->save();


                # Todo OK, regresamos éxito
                $response['success']['title']   = '¡Artículo creado!';
                $response['success']['message'] = 'Se ha creado el artículo.';
                $response['success']['url']     = route('admin.blog.edit',['id' => $article->id]);
            } catch (\Exception $e) {
                //print_r($e->getMessage()); exit;
                # Ocurrio un error, devolvemos mensajes.
                $response['error']['title']   = 'Oops, ocurrio un error.';
                $response['error']['message'] = 'Contacte al desarrollador para más información.';
                $response['error']['message'] = $e;
            }
        }

        return response()->json($response);
    }

    /**
     * Display form to edit article
     * 
     *  @return View
     */
    public function edit(Article $article)
    {
        $data['article'] = $article;
        return view('admin.blog.edit',$data);
    }

    /**
     * Update article
     * 
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $article = Article::findOrFail($id);
        $validator = \Validator::make(
            $request->all(),
            [
                'title'    => 'required',
                'excerpt'    => 'required',
                'status'    => 'required|numeric',
                'file-thumb'    => 'max:4096',
                'file-cover'    => 'max:4096',
            ],
            [
                'required'  => 'Campo requerido.',
                'max'  => 'El archivo supera el tamaño máximo permitido (4MB).'
            ]
        );

        if ($validator->fails()) {
            return response()->json($validator->messages(), 422);
            return $validator->messages();
        } else {
            $response = [];
            try {
                $article->title = $request->get('title','');
                $article->slug = $request->get('slug','');
                $article->excerpt = $request->get('excerpt','');
                $article->content = addslashes($request->get('content',''));
                $article->status = $request->get('status','');
                $article->created_at = $request->get('created_at','');
                
                # Guardamos archivos
                    $destinationPath = public_path().'/uploads/articles/';
                    // thumb
                    if($request->hasFile('file-thumb')){
                        $thumb = $request->file('file-thumb');
                        $filename = $article->id.'-thumb-'.time().'.'.$request->file('file-thumb')->extension();
                        $thumb->move($destinationPath, $filename);
                        $article->image = $filename;
                    }
                    // head cover
                    if($request->hasFile('file-cover')){
                        $cover = $request->file('file-cover');
                        $filename = $article->id.'-cover-'.time().'.'.$request->file('file-cover')->extension();
                        $cover->move($destinationPath, $filename);
                        $article->imageHead = $filename;
                    }

                # Guardamos para obtener el ID
                $article->save();


                # Todo OK, regresamos éxito
                $response['success']['title']   = '¡Artículo actualizado!';
                $response['success']['message'] = 'Se ha actualizado exitosamente la información del artículo.';
                $response['success']['url']     = 'reload';
            } catch (\Exception $e) {
                //print_r($e->getMessage()); exit;
                # Ocurrio un error, devolvemos mensajes.
                $response['error']['title']   = 'Oops, ocurrio un error.';
                $response['error']['message'] = 'Contacte al desarrollador para más información.';
                $response['error']['message'] = $e;
            }
        }

        return response()->json($response);
    }

    /**
     * Delete article
     */
    public function destroy(Request $request,$id)
    {
        try {
            $article = Article::findOrFail($id);

            # Guardamos para obtener el ID
            $article->forceDelete();


            # Todo OK, regresamos éxito
            $response['success']['title']   = '¡Artículo eliminado!';
            $response['success']['message'] = 'Se ha eliminado el artículo.';
            $response['success']['url']     = 'reload';
        } catch (\Exception $e) {
            //print_r($e->getMessage()); exit;
            # Ocurrio un error, devolvemos mensajes.
            $response['error']['title']   = 'Oops, ocurrio un error.';
            $response['error']['message'] = 'Contacte al desarrollador para más información.';
            $response['error']['message'] = $e;
        }
        return response()->json($response);
    }
}
