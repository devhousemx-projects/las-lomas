<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Area;
use App\Building;

class AdminController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $data['total'] = \Area::count();
        $data['available'] = \Area::where('status', true)->count();
        $data['sold'] = \Area::where('status', false)->count();
        $data['separated'] = \Area::where('status', 2)->count();

        return view('admin.index', $data);
    }

    /**
     * Lista Edificios
     */
    public function buildings()
    {
        $data['buildings'] = Building::orderBy('name')->get();

        # Estadísticas
        $data['total'] = Area::count();
        $data['available'] = Area::where('status', true)->count();
        $data['sold'] = Area::where('status', false)->count();
        $data['separated'] = Area::where('status', 2)->count();

        return view('admin.buildings.index', $data);
    }

    /**
     * Modifica el status del Edificio
     */
    public function setBuildingStatus(Request $request, $id)
    {
        $building = Building::find($id);
        $building->active = $request->get('active',0);
        $building->save();

        return redirect()->route('admin.buildings')->with('success', $building->name .' marcada como '.($building->active ? 'DISPONIBLE' : 'NO DISPONIBLE'));
    }

}
