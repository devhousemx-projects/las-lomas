<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Area;

class AdminAreaController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $data['areas'] = Area::orderBy('manzana')->orderBy('area')->get();

        # Estadísticas
        $data['total'] = Area::count();
        $data['available'] = Area::where('status', true)->count();
        $data['sold'] = Area::where('status', false)->count();
        $data['separated'] = Area::where('status', 2)->count();

        return view('admin.areas.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $area = Area::find($id);

        if (empty($area)) {
            return redirect()->back();
        }

        $data['area'] = $area;

        return view('admin.areas.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        $area = Area::find($id);

        $validator = \Validator::make(
            $request->all(),
            [
                'status' => 'required|numeric',
            ],
            [
                'required'               => 'Campo requerido.'
            ]
        );

        if ($validator->fails()) {
            return $validator->messages();
        } else {
            $response = [];
            try {
                $area->status          = $request->get('status');
                $area->price          = $request->get('price');

                # Guardamos para obtener el ID
                $area->save();

                # Todo OK, regresamos éxito
                $response['success']['title']   = 'Actualizado';
                $response['success']['message'] = 'Área actualizada correctamente';
                $response['success']['url']     = 'keep';
            } catch (\Exception $e) {
                # Ocurrio un error, devolvemos mensajes.
                $response['error']['title']   = 'Oops, hubo un error.';
                $response['error']['message'] = 'Contacte a soporte técnico para más información.';
            }
        }

        return response()->json($response);
    }

    /**
     * Actualiza el estado de una área (Web Service, integration with external software)
     */

    public function updateStatus(Request $request)
    {
        $response = [];
        $area = Area::where('area',$request->get('number',0))
            ->whereHas('level.building',function($q) use ($request){
                $q->where('id',$request->get('building',0));
            })
            ->firstOrFail();

        $newStatus = $request->get('status','');
        if(strlen($newStatus) && is_numeric($newStatus) && $newStatus >= 0 && $newStatus <= 2){
            $area->status = intval($newStatus);
            $area->save();
            $response['success']['title']   = 'Estado actualizado correctamente';
            $response['success']['message'] = 'Se ha actualizado exitosamente el status del área.';
        }
        else{
            $response['success']['title']   = 'Clave de status no válido';
            $response['success']['message'] = 'La clave de status de área no es correcto, corrobre y reintente.';
        }
        
        return response()->json($response);
    }

    /**
     * Modifica el status del área a  Disponible
     */
    public function available(Request $request, $id)
    {
        $area         = Area::find($id);
        $area->status = 1;
        $area->save();

        return redirect()->route('admin.areas')->with('success', 'Área '. $area->area .' marcada como disponible');
    }

    /**
     * Modifica el status del área a Reservado
     */
    public function separated(Request $request, $id)
    {
        $area         = Area::find($id);
        $area->status = 2;
        $area->save();

        return redirect()->route('admin.areas')->with('success', 'Área '. $area->area . ' marcada como reservada');
    }

    /**
     * Modifica el status del área a Vendido
     */
    public function sold(Request $request, $id)
    {
        $area         = Area::find($id);
        $area->status = 0;
        $area->save();

        return redirect()->route('admin.areas')->with('success', 'Área '. $area->area .' marcada como vendida');
    }
}
