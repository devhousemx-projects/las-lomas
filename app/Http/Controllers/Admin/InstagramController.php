<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Area;
use App\Building;

use App\Instagram;

class InstagramController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        # Estadísticas
        $data['total'] = Area::count();
        $data['available'] = Area::where('status', true)->count();
        $data['sold'] = Area::where('status', false)->count();
        $data['separated'] = Area::where('status', 2)->count();

        # Instagram
        $accessToken = 'ACCESS-TOKEN';

        $params = array(
            'get_code' => isset( $_GET['code'] ) ? $_GET['code'] : '',
            'access_token' => $accessToken,
            'user_id' => 'USER-ID'
        );
        $ig = new Instagram( $params );
        $data['instagram'] = $ig;
        $data['instagramUser'] = $ig->getUser();
        

        return view('admin.instagram.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function authorization()
    {
        //
    }
}
