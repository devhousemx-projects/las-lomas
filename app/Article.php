<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $hidden = ["updated_at"];
    public $timestamps = false;

    protected $dates = ['created_at'];
    protected $appends = ['thumbURL','coverURL','link'];
    
    public function scopeLast($query)
    {
        return $query->orderByDesc('created_at');
    }

    public function scopePublished($query)
    {
        return $query->where('status',1);
    }

    public function getThumbURLAttribute()
    {
        return asset('uploads/articles/'.$this->image);
    }

    public function getCoverURLAttribute()
    {
        return asset('uploads/articles/'.$this->imageHead);
    }
    public function getLinkAttribute()
    {
        return "/blog/".$this->slug;
    }
}
