<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    protected $appends = ['available_areas', 'available_esmeralda', 'available_zafiro', 'available_ambar', 'level_identifier', 'total_areas'];
    // protected $hidden = ["shape", "coords"];

    public function building() {
        return $this->belongsTo('App\Building', 'building_id');
    }

    public function areas() {
        return $this->hasMany('App\Area', 'level_id')->orderBy('area', 'ASC');
    }

    public function getTotalAreasAttribute() {
        return $this->areas()->count();
    }

    public function getAvailableAreasAttribute() {
        return $this->areas()->where('status', true)->count();
    }

    public function availableType($type) {
        $typeCode = \App\Type::where('code', $type)->first();
        return $this->areas()->where('status', true)->where('type_id', $typeCode->id)->count();
    }

    public function availableServiceType($type) {
        $typeCode = \App\Type::where('serviceType', $type)->get();
        return $this->areas()->where('status', true)->whereIn('type_id', $typeCode->pluck('id')->toArray())->count();
    }

    public function getAvailableAssistedLivingAttribute()
    {
        return $this->availableServiceType('ASSISTED LIVING');
    }

    public function getAvailableMemoryCareAttribute()
    {
        return $this->availableServiceType('MEMORY CARE');
    }

    public function getAvailableEsmeraldaAttribute()
    {
        return null;
        return $this->availableType('acacia');
    }

    public function getAvailableZafiroAttribute()
    {
        return null;
        return $this->availableType('alamo');
    }

    public function getAvailableAmbarAttribute()
    {
        return null;
        return $this->availableType('Arce');
    }

    public function getLevelIdentifierAttribute()
    {
        return $this->building_id . '-' . $this->level;
    }

}
