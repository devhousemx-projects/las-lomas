<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Building extends Model
{
    public $timestamps = false;
    protected $appends = ['stats'];

    public function levels()
    {
        return $this->hasMany('App\Level', 'building_id');
    }

    public function getStatsAttribute()
    {
        $stats = new \StdClass();
        $stats->total = $this->levels->reduce(function($counter,$level){ return $counter + count($level->areas); });
        $stats->levels = $this->levels;
        //$stats->uses = '';
        //if(count($stats->levels) && count($this->levels->first()->areas))
            //$stats->uses = ucwords(strtolower($this->levels->first()->areas->first()->type->serviceType));
        $stats->freeTotal = $this->levels->reduce(function($counter,$level){ return $counter + $level->availableAreas; });
        /*$stats->freeAssistedLiving = $this->levels->reduce(function($counter,$level){ return $counter + $level->availableAssistedLiving; });
        $stats->freeMemoryCare = $this->levels->reduce(function($counter,$level){ return $counter + $level->availableMemoryCare; });*/
        return $stats;
    }
}
