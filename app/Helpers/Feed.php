<?php namespace App\Helpers;

date_default_timezone_set('America/Mexico_City');
setlocale(LC_TIME, 'es_ES', 'Spanish_Spain', 'Spanish');  

define('CACHE_FB_POSTS_DURATION', 2880);

/**
* Feed de Facebook
*/
class Feed
{
	private $api;
	private $pageId;
	private $fb;

	public $details;
	public $access_token;

	function __construct()
	{
		# Configuración de la API Facebook
		$this->api = [
		  'app_id' => env('FACEBOOK_APP_ID',''),
		  'app_secret' => env('FACEBOOK_APP_SECRET',''),
		  'default_graph_version' => 'v2.10',
		  'default_access_token' => env('FACEBOOK_ACCESS_TOKEN',''),
		];

		$this->pageId = env('FACEBOOK_PAGE_ID','');
		
        # Contexto de la API
        try {
	        $response = new \Facebook\Facebook($this->api);
			$this->fb = $response;
        } catch (Exception $e) {
        	
        }
		
		$this->details = $this->getPageDetails();
		$this->accesss_token = $this->api['default_access_token'];
	}
	

	public function getFeed($limit = 3, $rawResults = false, $debug = false) {		
		$cacheKey = 'fb_posts_' . $limit . ($rawResults ? '_raw' : '');
		if($debug)  {
			$limit = 3;
			\Cache::forget($cacheKey);
			//dd('purged');
		}
		
		if(!\Cache::has($cacheKey)) {
			try {
				
				# Check out the fields: https://developers.facebook.com/docs/graph-api/reference/v2.5/page/feed
				$request = $this->fb->get('/'.$this->pageId.'/feed?fields=created_time,message,permalink_url,full_picture&type=photo&limit='.$limit);
				
				$feedEdge = $request->getGraphEdge();
				
				$posts = [];
				$iterations = 1;
				
				$self = $this;
				
				$getPosts = function ($data) use ($self, $rawResults, &$feedEdge, $debug, &$posts, &$iterations, &$getPosts) {
					$message = '';
					# Iteramos cada post
					foreach ($data as $key => $post)  {	
						if(isset($post['message'])) {
							# Convertimos URLs y hashtags a enlaces
							$message = preg_replace('"\b(http://\S+)"', '<a href="$1" target="_blank">$1</a>', $post->getProperty('message'));
							$message = preg_replace('/#([\p{Pc}\p{N}\p{L}\p{Mn}]+)/u', '<a href="https://www.facebook.com/hashtag/$1" target="_blank">#$1</a>', $message);
						}

						# Segmentamos el texto de la publicación hasta encontrar el emoji de la nieve
						$parts = explode('\n', utf8_encode($message));
						
						# Guardamos la información del post con el siguiente formato
						$posts[] = [
							'id' => explode('_', $post->getProperty('id'))[1],
							'content'  => !$rawResults ? $message : $post->getProperty('message'),
							'media' => $post->getProperty('full_picture'),
							'link'  => $post->getProperty('permalink_url'),
							'date'  => $post->getField('created_time')->format('Y-m-d H:i:s'),
						];
					}

					$nextData = $self->fb->next($data);

					// if($iterations == 2) dd($posts);
					// $iterations++;

					if(!empty($nextData) && !$debug) return $getPosts($nextData);

				};

				$getPosts($feedEdge);

				if(!empty($posts))
					\Cache::put($cacheKey, $posts, CACHE_FB_POSTS_DURATION);
				else
					\Cache::forget($cacheKey);
			} catch (FacebookRequestException $e) {
				dd($e->getMessage());
				# La Graph API regresó un error
			} catch (\Exception $e) {
				# Ocurrió algun otro error
				dd($e->getMessage());
			}
		}

		return \Cache::get($cacheKey);
	}
	
	public function getPageDetails()
    {
		if(!\Cache::has('fb_details')) {
			try {
				$request = $this->fb->get('/'. $this->pageId .'/?fields=picture,name,link');
			} catch (Exception $e) {
				return false;
			}
   
			$node = $request->getGraphNode();
   
			$details = [
			   'picture' => $node->getField('picture')->getField('url'),
			   'name' => $node->getField('name'),
			   'url' => $node->getField('link')
			];

			\Cache::forever('fb_details', $details);
		}
		 
		return \Cache::get('fb_details');
	}
	
	public function getPostImage($postId)
	{
		try {
			// Returns a `Facebook\FacebookResponse` object
			$response = $this->fb->get(
			  '/'.$this->pageId .'_'. $postId .'/?fields=full_picture'
			);
		  } catch(Facebook\Exceptions\FacebookResponseException $e) {
			echo 'Graph returned an error: ' . $e->getMessage();
			exit;
		  } catch(Facebook\Exceptions\FacebookSDKException $e) {
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			exit;
		  }
		  $graphNode = $response->getGraphNode();
		  
		  return $graphNode->getField('full_picture');
		  /* handle the result */
	}
}