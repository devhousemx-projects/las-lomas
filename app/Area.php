<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $hidden = ["created_at", "updated_at", 'level_id', 'type_id', 'price', 'rooms', 'bathrooms'];
    public $timestamps = false;
    public $appends = ['price_formatted', 'description', 'rooms', 'bathrooms', 'type_code'];
    
    public function level() {
        return $this->belongsTo('App\Level', 'level_id');
    }

    public function type()
    {
        return $this->belongsTo('App\Type', 'type_id');
    }

    public function getDescriptionAttribute()
    {
        return $this->type->description;
    }

    public function getPriceFormattedAttribute() {
        return number_format($this->price, 2, '.', ',');
    }

    public function getRoomsAttribute()
    {
        return $this->type->rooms;
    }

    public function getBathroomsAttribute()
    {
        return $this->type->bathrooms;
    }

    public function getTypeCodeAttribute()
    {
        return $this->type->code;
    }
}
