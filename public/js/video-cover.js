function VideoCover(options = {}) {
    options = {
        autoplay: options?.autoplay ?? false,
        target: options?.target ?? '',
        hideOnScroll : options?.hideOnScroll ?? false,
        onVideoEnds : options?.onVideoEnds ?? null,
    };
    let $targets;
    let $covers = [];

    const init = () => {
        const { target } = options;
        if(!target.length) return;
        $targets = document.querySelectorAll(target);
        $targets.forEach($target => {
            $covers.push(new Cover($target,options));
        })
        return $covers;
    }

    return init();
}

function Cover (target,options){
    const targetID = target.id;
    const $video = target.querySelector('.video-cover--video') ?? null;
    const $playButtons = target.querySelectorAll(`.video-cover--button`) ?? null;
    const $caption = document.querySelector(`.video-cover--caption[data-rel="#${targetID}"]`);
    var isPlaying = false;

    if(!$video || !$playButtons) return;

    $video.style.display = 'none';

    const playVideo = () => {
        if(isPlaying) return;

        $video.muted = true;
        $video.play();
        $video.style.display = 'block';
        if($caption)
            $caption.style.display = 'none';
        $playButtons.forEach(button => button.style.display = 'none');
        isPlaying= true;
    }
    const pauseVideo = () => {
        if(!isPlaying) return;

        $video.muted = false;
        $video.pause();
        $video.style.display = 'none';
        if($caption)
            $caption.style.display = 'block';
        $playButtons.forEach(button => button.style.display = 'block');
        isPlaying = false;
    }
    
    const init = () => {
        $video.pause();
        $playButtons.forEach(button => {
            button.addEventListener('click',function(){
                if(isPlaying)
                    pauseVideo();
                else
                    playVideo();
            })
        });
        if(options.hideOnScroll)
            window.addEventListener('scroll', pauseVideo);
        if(typeof options.onVideoEnds === "function")
            $video.addEventListener('ended',options.onVideoEnds,false);
        if(options.autoplay)
            playVideo();
    }

    init();

    return {
        playVideo,
        pauseVideo,
        target,
    }
}