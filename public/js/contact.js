window.addEventListener('DOMContentLoaded', (event) => {

    function sendData(form,options) {
        options = options || {
            onSuccess: null,
            onError: null,
        };
        const XHR = new XMLHttpRequest();       
    
        // Bind the FormData object and the form element
        const FD = new FormData( form );
    
        // Define what happens on successful data submission
        /*XHR.addEventListener( "load", function(event) {
          console.log( event.target.responseText );
        } );*/
    
        // Define what happens in case of error
        XHR.addEventListener( "error", function( event ) {
          console.log( 'Oops! Something went wrong.' );
        } );
    
        // Set up our request
        const action = form.getAttribute('action');
        const csrfToken = document.querySelector('meta[name="csrf-token"]')['content'];
        XHR.open( "POST", action );
        XHR.setRequestHeader('X-CSRF-TOKEN', csrfToken);
    
        // The data sent is what the user provided in the form
        XHR.send( FD );
        XHR.onreadystatechange = function (aEvt) {
            if (XHR.readyState == 4) {
                if(XHR.status == 200){
                    console.log(XHR.responseText);
                    if(typeof options.onSuccess === "function")
                        options.onSuccess();
                }
                else{
                    console.log("Error loading page\n");
                    if(typeof options.onError === "function")
                        options.onError();
                }
            }
        };
    }
    
    // Access the form element...
    document.querySelectorAll('.frm-contact').forEach($form => {
        $form.addEventListener('submit',function(e){
            e.preventDefault();
            const $submitButton = $form.querySelector('button');
            sendData($form,{
                onSuccess:  () => {
                    $submitButton.textContent = '¡MENSAJE ENVIADO!';
                    setTimeout(() => {
                        $form.reset();
                        $submitButton.textContent = 'ENVIAR';
                    },3000);
                },
                onError: () => {
                    $submitButton.textContent = 'Error al enviar, reintente.';
                    setTimeout(() => {
                        $submitButton.textContent = 'ENVIAR';
                    },3000);
                }
            });
        });
    });
});