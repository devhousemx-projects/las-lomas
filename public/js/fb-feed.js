// Facebook feed

/**
 * Require a templete like this in the HTML content:
 */
/**
<template id="feed-post">
    <div class="feed-post">
        <a href="" target="_blank" class="feed-post--page-link"><span class="feed-post--logo"></span></a>
        <div class="feed-post--card-title">
            <a href="#" target="_blank" class="feed-post--page-link"><div class="feed-post--avatar"><span></span></div></a>
            <div class="feed-post--meta">
                <a href="#" target="_blank" class="feed-post--page-link"><b><span class="feed-post--page-name"></span></b></a><br />
                <span class="feed-post--date"></span>
            </div>
        </div>
        <a href="#" target="_blank" class="feed-post--post-link"><div class="feed-post--thumb"></div></a>
        <div class="feed-post--content">
        </div>
    </div>
</template>
**/

const FBFeed_templateID = '#feed-post';
const FBFeed_postsContainer = '.feed-posts';
const API_endpoint = 'apiv1/json/posts';
const DEMO_title = "(Demo) Las Lomas Habitat + Golf";
const isDemo = false;

const loadPosts = (data) => {
    const page = data.profile;
    const container = document.querySelector(FBFeed_postsContainer);
    data.posts.map((post) => {
        const template = document.querySelector(FBFeed_templateID);
        const el = document.createElement('div');
        const date = new Date(post.date);
        const options = { year: 'numeric', month: 'long', day: 'numeric' };

        el.append(template.content.cloneNode(true));
        el.classList.add('col-md-4');

        el.querySelector('.feed-post--avatar span').style.backgroundImage = `url("${page.picture}")`;
        if(isDemo)
            el.querySelector('.feed-post--page-name').textContent = DEMO_title;
        else
            el.querySelector('.feed-post--page-name').textContent = page.name;
        el.querySelector('.feed-post--date').textContent = date.toLocaleDateString('es-ES', options);
        el.querySelector('.feed-post--thumb').style.backgroundImage = `url("${post.media}")`;
        el.querySelector('.feed-post--content').textContent = post.content;
        el.querySelectorAll('.feed-post--page-link').forEach(link => link.setAttribute('href',page.url) );
        el.querySelectorAll('.feed-post--post-link').forEach(link => link.setAttribute('href',post.link) );
        container.appendChild(el);
    })
}
window.addEventListener('DOMContentLoaded', (event) => {
    fetch(API_endpoint)
        .then(response => response.json())
        .then(data => loadPosts(data) );
});