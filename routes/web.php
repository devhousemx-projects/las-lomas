<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// API Routes
Route::group(['prefix' => 'apiv1'], function() use ($router) {
    Route::get('/interactive', function() {
        return App\Building::with(['levels' => function($q){
            $q->with('areas');
        }])->get();
    });
    Route::get('/prototypes', function() {
        return App\Type::get();
    });
    Route::get('/json/posts',['as' => 'json.posts', 'uses' => 'FacebookController@feed']);
    Route::post('/email/send/{type}',['as' => 'email.send', 'uses' => 'ContactController@send']);
});


# Admin routes
Route::group(['prefix' => 'panel'], function() {
	Auth::routes();
});

Route::group(['prefix' => 'panel', 'namespace' => 'Admin', 'middleware' => 'auth'], function() {
//Route::group(['prefix' => 'panel', 'namespace' => 'Admin',], function() {
    Route::get('/', ['as' => 'admin.areas', 'uses' => 'AdminAreaController@index']);
	# Areas
    // Route::get('areas', ['as' => 'admin.areas', 'uses' => 'AdminAreaController@index']);
    Route::get('area/edit/{id}', ['as' => 'admin.areas.edit', 'uses' => 'AdminAreaController@edit']);
    Route::post('area/update/{id}', ['as' => 'admin.areas.update', 'uses' => 'AdminAreaController@update']);
    Route::get('area/available/{id}', ['as' => 'admin.areas.available', 'uses' => 'AdminAreaController@available']);
    Route::get('area/separated/{id}', ['as' => 'admin.areas.separated', 'uses' => 'AdminAreaController@separated']);
    Route::get('area/sold/{id}', ['as' => 'admin.areas.sold', 'uses' => 'AdminAreaController@sold']);
    Route::get('areas', ['as' => 'sales.module', 'uses' => 'AdminAreaController@index']);
    Route::get('buildings', ['as' => 'admin.buildings', 'uses' => 'AdminController@buildings']);
    Route::get('buildings/status/{id}', ['as' => 'admin.buildings.setStatus', 'uses' => 'AdminController@setBuildingStatus']);
});

Route::get('/', 'LandingController@index')->name('home');
Route::get('/disponibilidad', 'AvailabilityModuleController@index')->name('salesModule');
Route::get('/disponibilidad/generate', 'AvailabilityModuleController@generateTiles')->name('salesModuleGenerator');
