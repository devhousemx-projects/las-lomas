export const calculateCenterPoint = coords => {
    var maxX = 0,
      minX = Infinity,
      maxY = 0,
      minY = Infinity;
  
    // note: using Array.prototype.forEach instead of calling forEach directly
    // on "areas" so it will work with array-like objects, e.g. jQuery
  
    Array.prototype.forEach.call(coords, function(e) {
      var i = 0;
  
      while (i < coords.length) {
        var x = parseInt(coords[i++], 10),
          y = parseInt(coords[i++], 10);
  
        if (x < minX) minX = x;
        else if (x > maxX) maxX = x;
  
        if (y < minY) minY = y;
        else if (y > maxY) maxY = y;
      }
    });
  
    return {
      x: minX + (maxX - minX) / 2,
      y: minY + (maxY - minY) / 2
    };
  };
  
  export const validateEmail = emailAddress => {
    return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(emailAddress)
  }
  
  export const currencyFormat = ( amount ) => {
    return parseFloat(amount).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
  }
  