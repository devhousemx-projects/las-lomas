import axios from "axios";

//console.log("NODE_ENV",process.env.NODE_ENV);
const DOMAIN = window.location.origin;

//export const EXTERNAL_API_GET = 'lotificacion-infoLaPorta.php';
//export const EXTERNAL_API_GET = 'https://canadaslaporta.com/lotificacion-info.php';

export const API_URL =
  process.env.NODE_ENV === "production"
    ? "apiv1/"
    //: DOMAIN + "/las-lomas/public/apiv1/"
    : DOMAIN + "/renderINC/las-lomas/public/apiv1/"

export const instance = axios.create({
  baseURL: API_URL
});

//export default instance;



// ONLY FOR TEST PURPOSES
export const DEMO_EXTERNAL_API_RESPONSE = [{}];
