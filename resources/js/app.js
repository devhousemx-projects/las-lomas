require('./bootstrap');

import axios from "axios";
import { API_URL, DEMO_EXTERNAL_API_RESPONSE, instance } from "./utils/instance";
import { calculateCenterPoint, validateEmail, currencyFormat } from "./utils/functions";

import 'ol/ol.css';
import Feature from 'ol/Feature';
import Map from 'ol/Map';
import Overlay from 'ol/Overlay';
import { Tile as TileLayer, Vector as VectorLayer } from 'ol/layer';
//import TileLayer from 'ol/layer/Tile';
import VectorSource from 'ol/source/Vector';
import Point from 'ol/geom/Point';
import View from 'ol/View';
import Zoomify from 'ol/source/Zoomify';
import { defaults as defaultControls } from 'ol/control';
import { Icon, Style, Text, Circle, Fill, Stroke } from 'ol/style';
import { fromLonLat,toLonLat } from 'ol/proj';
import { getVectorContext } from 'ol/render';
import { boundingExtent,getCenter } from 'ol/extent';

//import './map';


const Interactive = (function() {

  let areaPrototypes = [];
  let blockAreas = [];
  let APIareas = [];
  let quoteAccess = false;
  let vectorLayer;
  let currentProperty;
  var emailAddress;

  var lastScrollTop = 0;
  var Rellax = require('rellax');
  var animation_elements = document.querySelectorAll('.animate');

  // Fetch data from API (prototypes)
  const getPrototypesInfo = ($dev = false) => {
    if (!$dev) return instance("/prototypes");
    //if (!$dev) return instance("");
    else {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          resolve(true);
        }, 0);
      });
    }
  };

  // Fetch data from API (areas)
  const getLevelsInfo = ($dev = false) => {
    if (!$dev) return instance("/interactive");
    //if (!$dev) return instance("");
    else {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          resolve(true);
        }, 0);
      });
    }
  };

  // Fetch data from API (areas)
  const getCRMInfo = ($dev = false) => {
    if (!$dev) return fetch(API_URL, {
	        'mode': 'cors',
	        'headers': {
            	'Access-Control-Allow-Origin': '*',
        	}
    	});
    return new Promise((resolve, reject) => {
        setTimeout(() => {
          resolve(DEMO_EXTERNAL_API_RESPONSE);
        }, 5000);
      });
    //return axios.get(EXTERNAL_API_GET);
  };

  const updateAreasData = (data) => {
    blockAreas[0].levels[0].areas.forEach((area) => area.APIdata = data.filter((APIarea) => {return APIarea.manzana == area.manzana && APIarea.lote == area.lote} )[0] || null);
    //console.log('areas updated!',blockAreas[0].levels[0].areas);
    //vectorLayer.changed();
    return true;
  }

  /***********************************************  
  Interactive init
  ***********************************************/

  const init = (options = {},callback) => {

    //  Fetch levels data from API (prototypes)
    getPrototypesInfo()
      .then(response => {
        areaPrototypes = response.data;
      });

    //  Fetch levels data from API (areas)
    getLevelsInfo()
      .then(response => {
        blockAreas = response.data;
        console.log("blockAreas",blockAreas);
        
        globalAreaInit();
        //  Fetch levels data from API (prototypes)
        /* getCRMInfo()
          .then(response => response.json())
          .then(data => {
            APIareas = data;
            if(updateAreasData(APIareas))
              globalAreaInit();
          })
          .catch(() => {
            console.log("Can’t access " + EXTERNAL_API_GET + " response. Blocked by browser?")
          }) */
      });
    
    /**************************************************************************
    *  Zoomify config
    ***************************************************************************/

    var imgWidth = 8467;
    var imgHeight = 7987;

    var zoomifyUrl = 'images/simulator/tiles/';

    var source = new Zoomify({
      url: zoomifyUrl,
      size: [imgWidth, imgHeight],
      crossOrigin: 'anonymous',
      zDirection: -1, // Ensure we get a tile with the screen resolution or higher
    });
    var extent = source.getTileGrid().getExtent();

    var retinaPixelRatio = 2;
    var retinaSource = new Zoomify({
      url: zoomifyUrl,
      size: [imgWidth, imgHeight],
      crossOrigin: 'anonymous',
      zDirection: -1, // Ensure we get a tile with the screen resolution or higher
      tilePixelRatio: retinaPixelRatio, // Display retina tiles
      tileSize: 256 / retinaPixelRatio, // from a higher zoom level
    })

    var layer = new TileLayer({
      source: source,
    });

  const globalAreaInit = (callback) => {
    
    /**************************************************************************
    *  Map init
    ***************************************************************************/
    
    var map = new Map({
      layers: [layer],
      target: 'map',
      controls: defaultControls(),
      view: new View({
        // adjust zoom levels to those provided by the source
        resolutions: layer.getSource().getTileGrid().getResolutions(),
        // constrain the center: center cannot be set outside this extent
        extent: extent,
        constrainOnlyCenter: true,
        //minZoom: 2,
      }),
    });
    map.getView().fit(extent);


    /**************************************************************************
    *  Points Generation
    ***************************************************************************/

    var initialZoom = map.getView().getZoom();
    map.on('moveend', function(e) {
      var newZoom = map.getView().getZoom();
      if (initialZoom != newZoom) {
        initialZoom = newZoom;
      }
    });
    
    var context = {
      getSize: function(feature) {
        let radio = feature.getProperties().attributes["radio"];        
        radio = radio / 12.0;									
        radio = radio * (1.0 / map.getView().getResolution()) * 12;
        return radio;
      },
      getSizeFont: function(feature) {
        let radio = feature.getProperties().attributes["radio"];        
        radio = radio / 12.0;									
        radio = radio * (1.0 / map.getView().getResolution()) * 12;
        return radio;
      },
      getLabel: function(feature) {
        return feature.getProperties().attributes["lote"];
      },
      getColor: function(feature) {
        let color = '#353535';
        let statusID = null;
        if(feature.getProperties().attributes['APIdata']){
          //console.log('APIdata found!');
          statusID = feature.getProperties().attributes['APIdata']['estatus'] || 0;
        }
        else {
          statusID = feature.getProperties().attributes['status'] ?? 0;
        }
        switch(parseInt(statusID)){
          case 0: color = '#f71b16'; break; // sold
          case 1: color = '#55fb4e'; break; // available
          case 2: color = '#f5c929'; break; // reserved
          default: break;
        }
        return color;
      },
      getDisplay: function(feature) {
        let radio = feature.getProperties().attributes["radio"];        
        radio = radio / 12.0;									
        radio = radio * (1.0 / map.getView().getResolution()) * 12;

        if (radio < 4) return "none";
        return "";
      }
    };

    const features = [];    

    const areas = blockAreas.reduce(
      ($buildings,building) => {
        return building.levels.reduce(
          ($buildings,level) => {
            return level.areas.reduce(
              ($buildings,area) => {
                if(area.coords.length){
                  const { x, y } = calculateCenterPoint(area.coords.split(","));
                  features.push(new Feature({
                    geometry: new Point([x,-y]),
                    attributes: {
                      ...area,
                      radio: 30
                    }
                  }));
                }
                return $buildings;
              }, []
            )
          }, []
        )
      }, []
    );
    
    // create the source and layer for random features
    const vectorSource = new VectorSource({
      features
    });
    vectorLayer = new VectorLayer({
      source: vectorSource,
      style: function(feature, resolution){
        if(context.getDisplay(feature) === "none")
          return null;
        return new Style({
          image: new Circle({
            radius: context.getSize(feature),
            stroke: new Stroke({
              color: "white",
              width: context.getSize(feature)/12,
            }),
            fill: new Fill({
              color: context.getColor(feature)
            }),
          }),
          text: new Text({
              text: `${context.getLabel(feature)}`,
              font: `${context.getSize(feature)*14/12}px "Open Sans", "Arial Unicode MS", "sans-serif"`,
              placement: 'point',
              fill: new Fill({color: '#fff'}),
          }),
          display: `${context.getDisplay(feature)}`
        })
      }
      /*style: new Style({
        image: new Circle({
          radius: 10,
          fill: new Fill({color: 'red'})
        })
      })*/
    });
    map.addLayer(vectorLayer);


    /**************************************************************************
    *  Popup
    ***************************************************************************/

    var popupContainer = document.getElementById('popup-quickview');

    var popup = new Overlay({
      element: popupContainer,
      //positioning: 'bottom-center',
      //stopEvent: false,
      //offset: [0, -50],
      autoPan: true,
      autoPanAnimation: {
          duration: 250
      }
    });
    map.addOverlay(popup);

    /* Add a pointermove handler to the map to render the popup.*/
    map.on('click', function (evt) {
      popupContainer.style.display = 'block';
      popupContainer.classList.add('animate__animated','animate__bounceIn');

      var feature = map.forEachFeatureAtPixel(evt.pixel, function (feat, layer) {
          return feat;
      });

    
      if (feature && feature.getGeometry().getType() == 'Point') {
        currentProperty = feature.getProperties().attributes;
        document.querySelectorAll('.display-area').forEach( element => element.innerHTML = feature.getProperties().attributes['lote'] ?? '' );
        document.querySelectorAll('.display-surface').forEach( element => element.innerHTML = feature?.getProperties()?.attributes['superficie'] ?? '' );
        document.querySelectorAll('.display-block').forEach( element => element.innerHTML = feature.getProperties().attributes['manzana'] ?? '' );
        document.querySelectorAll('.display-frontSize').forEach( element => element.innerHTML = feature.getProperties().attributes['frente'] ?? '' );
        document.querySelectorAll('.display-reference').forEach( element => element.innerHTML = feature.getProperties().attributes['clave_catastro'] ?? '' );
        document.querySelectorAll('.display-delivery').forEach( element => element.innerHTML = feature.getProperties().attributes['fecha_entrega'] ?? 'No definida' );
        document.querySelectorAll('.display-listPrice').forEach( element => element.innerHTML = currencyFormat(feature.getProperties().attributes['precio_lista']) ?? '' );
        let price = parseFloat(feature.getProperties().attributes['superficie']) * parseFloat(feature.getProperties().attributes['precio_lista'] ?? 0);
        document.querySelectorAll('.display-price').forEach( element => element.innerHTML = currencyFormat(price) );
        let currentStatus = parseInt(feature.getProperties().attributes['status'] ?? 0);
        if(currentStatus == 1){
          document.querySelectorAll('.area-resume__button').forEach( element => element.style.display = 'block' )
          if(currentStatus != 1){
            document.querySelectorAll('.area-resume__button__payment').forEach( element => element.style.display = 'none' )
          }
        }
        else{
          document.getElementById('require-quote').removeAttribute('style');
          document.querySelectorAll('.area-resume__button').forEach( element => element.style.display = 'none' )
        }

        var bodyTable = ``;

        var billingTable = document.getElementById('tbl-billing');
        billingTable.getElementsByTagName("tbody")[0].innerHTML = bodyTable;
        

        var coordinate = evt.coordinate;    //default projection is EPSG:3857 you may want to use ol.proj.transform
        popup.setPosition(coordinate);

        // define the event
        let event = new CustomEvent('propertySelected');
        // add your custom arguments
        event.property = currentProperty;
        //window.addEventListener('propertySelected', function (e) {console.log(e)});
        // fire it
        window.dispatchEvent(event);
      }
      else {
        currentProperty = null;
        popup.setPosition(undefined);            
      }
    });

  }

    // Animate
        animation_elements.forEach(element => element.style.opacity = 0 );
        check_if_in_view();
        window.addEventListener('scroll',check_if_in_view);
        window.addEventListener('resize',check_if_in_view);

        // menu
        lastScrollTop = 0;
        window.addEventListener("scroll", function(){
            var st = window.pageYOffset || document.documentElement.scrollTop;
            if(st > 0){
                /*if (st > lastScrollTop)
                    toggleHeader(false);
                else*/
                    toggleHeader(true);
            }
            else
                toggleHeader(false);
            lastScrollTop = st <= 0 ? 0 : st; // For Mobile or negative scrolling
        }, false);

        // Rellax
        window.addEventListener('load', (event) => {
            setTimeout(function(){
                console.log('Rellax init..');
                var rellax = new Rellax('.rellax', {
                    center: true,
                    breakpoints:[576, 768, 1201],
                }); 

            },2000);
            console.log('The page has fully loaded');
        });

        // Carousel
        var myCarousel = document.querySelector('#myCarousel')
        var carousel = new bootstrap.Carousel(myCarousel)

        // Simulator
        /*Interactive.init({},function() {
            // Actions to do when a building is clicked
        });*/

        // Google Maps
        //console.log('Google map init call');
        //window.CustomGoogleMap.init();



  }

  /**************************************************************************
  *  Require quote
  ***************************************************************************/
  var requireQuoteWrapper = document.getElementById('require-quote');
  var showBillingButton = document.getElementById('btn-showBilling');
  var emailInput = document.getElementById('require-quote-email');
  const showQuoteForm = function(){
    showBillingButton.style.display = 'none';
    requireQuoteWrapper.classList.add('animate__animated','animate__slideIn');
    requireQuoteWrapper.style.display = 'block';
  }

  emailInput.onkeyup = function(e) {
    e = e || window.event;
    var input = this;
    if(input.classList.contains('has-error'))
      checkEmailAddress(input.value)
  }

  const checkEmailAddress = function(emailAddress){
    emailInput.classList.remove('has-error','animate__animated','animate__headShake');
    if(!emailAddress.length){
      emailInput.classList.add('animate__animated','animate__headShake');
      return false;
    }
    if(!validateEmail(emailAddress)){
      emailInput.classList.add('has-error','animate__animated','animate__headShake');
      return false;
    }
    return true;
  }

  const submitEmail = (event) => {
    emailAddress = emailInput.value;
    
    if(!checkEmailAddress(emailAddress))
      return false;
    
    axios({
      method: 'POST',
      url: API_URL+'email/send/register',
      data: {
        email: emailAddress,
        manzana: currentProperty['manzana'],
        lote: currentProperty['lote'],
      },
    })
    .then(function (response) {
        //handle success
    })
    .catch(function (response) {
        //handle error
    });

    quoteAccess = true;
    requireQuoteWrapper.style.display = 'none';
    showBillingButton.style.display = 'block';
    openReserveModal();
    //openBillingModal();
  }

  const openBillingModal = (event) => {
    if(!quoteAccess){
      showQuoteForm();
      return false;
    }
    const billingModal = document.getElementById('modal-billing');
    billingModal.classList.add('animate__animated','animate__bounceIn');
    billingModal.style.display = 'block';
  }

  const openReserveModal = (event) => {
    if(!quoteAccess){
      showQuoteForm();
      return false;
    }
    const reserveModal = document.getElementById('modal-reserve');
    reserveModal.classList.add('animate__animated','animate__bounceIn');
    reserveModal.style.display = 'block';
  }

  const reserve = (event) => {
    var reserveCloseButton = document.getElementById('appointment-form').closest('.modal-content').querySelector('.btn-close');
    var reserveForm = document.getElementById('appointment-form');
    var reserveConfirmation = document.getElementById('appointment-response');
    var clientName = document.querySelector('[name="name"]').value;
    var clientPhone = document.querySelector('[name="phone"]').value;
    var clientComments = document.querySelector('[name="comment"]').value;
    axios({
      method: 'POST',
      url: API_URL+'email/send/reserve',
      data: {
        email: emailAddress,
        manzana: currentProperty['manzana'],
        lote: currentProperty['lote'],
        name: clientName,
        phone: clientPhone,
        comments: clientComments,
      },
    })
    .then(function (response) {
        //handle success
    })
    .catch(function (response) {
        //handle error
    });
    reserveForm.style.display = 'none';
    reserveConfirmation.classList.remove('animate__animated','animate__fadeOut');
    reserveConfirmation.classList.add('animate__animated','animate__fadeIn');
    reserveConfirmation.style.display = 'block';
    setTimeout(function(){
      reserveForm.style.display = 'block';
      reserveConfirmation.classList.remove('animate__animated','animate__fadeIn');
      reserveConfirmation.classList.add('animate__animated','animate__fadeOut');
      reserveConfirmation.style.display = 'none';
      reserveCloseButton.click();
    },5000)
  }

  const postRedirect = (path, params, method) => {
    path = path || 'https://apartados.investti.com';
    method = method || 'post';
    params = {
      id: currentProperty['APIdata']['id'],
      site: 'CDA'
    }

    var form = document.createElement('form');
    form.setAttribute('method', method);
    form.setAttribute('action', path);


    for (var key in params) {
        if (params.hasOwnProperty(key)) {
            var hiddenField = document.createElement('input');
            hiddenField.setAttribute('type', 'hidden');
            hiddenField.setAttribute('name', key);
            hiddenField.setAttribute('value', params[key]);

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
  }

  // Menu
    const toggleMenu = () => {
        var menuButtons = document.querySelectorAll('.btn-menu');
        var navbar = document.querySelector('.navbar');
        navbar.classList.toggle('active');
        if(navbar.classList.contains('active'))
            menuButtons.forEach((button) => {
                button.classList.add('active');
            });
        else
            menuButtons.forEach((button) => {
                button.classList.remove('active');
            });
    }

    const toggleConceptView = () => {
        let container = document.querySelector('.concept-view');
        let hotspots = document.querySelectorAll('.concept-view--point');
        if(container.classList.contains('active')){
            container.classList.remove('active')
            hotspots.forEach(function(item,index){
                item.classList.remove('animated','active');
            })
        }
        else{
            container.classList.add('active')
            hotspots.forEach(function(item,index){
                item.classList.add('animated','active');
            })
            check_if_in_view();
        }
    }


    function toggleHeader(display){
        var header = document.querySelector('.main-header');
        if(display){
            if(!header.classList.contains('fixed'))
                header.classList.add('fixed');
        }
        else
            header.classList.remove('fixed');
    }

    function scrollDown(selector){
        selector = selector || null;
        var element = null;
        if(!selector){
            selector = '.inner-section';
            element = document.querySelector(selector);
            console.log(element);
        }
        else
            element = document.querySelector(selector);
        // smooth scroll to element and align it at the bottom
        element.scrollIntoView({ behavior: 'smooth', block: 'start'});   

        // hide mobile menu
        let menuButton = document.querySelector('.btn-menu')
            console.log('Menu trigger click!');
        if(menuButton.classList.contains('active') && menuButton.offsetWidth > 0 && menuButton.offsetHeight > 0){
            menuButton.click();
        }
    }

    function scrollTop(){
        window.scrollTo({top: 0, behavior: 'smooth'});
    }

    function check_if_in_view() {
        var window_height = window.innerHeight;
        var window_top_position = window.scrollY;
        var window_bottom_position = (window_top_position + window_height);

        animation_elements.forEach(element => {
            let element_height = element.offsetHeight;
            let element_rect = element.getBoundingClientRect();
            const scrollTop = window.pageYOffset || document.documentElement.scrollTop;
            let element_top_position = element_rect.top + scrollTop;
            let element_bottom_position = (element_top_position + element_height);

            //check to see if this current container is within viewport
            if ((element_bottom_position >= window_top_position) &&
                (element_top_position <= window_bottom_position)) {
                //alert(element.data('animationDelay'));
                //console.log(`COND: ((${element_bottom_position} >= ${window_top_position}) &&
                //(${element_top_position} <= ${window_bottom_position}))`)
                /*console.log("window_height",window_height);
                console.log("window_top_position",window_top_position);
                console.log("window_bottom_position",window_bottom_position);
                console.log("element_top_position",element_top_position);
                console.log("element_bottom_position",element_bottom_position);*/
                var delay = (element.dataset.animationDelay !== 'undefined') ? element.dataset.animationDelay : 0;
                setTimeout(function () {
                    //console.log(element,'animated!');
                    element.classList.add('animated',element.dataset.animation);
                    element.removeAttribute('style');
                }, delay);
            } else {
                if(element.classList.contains('animate-reverse'))
                    element.classList.remove('animated',element.dataset.animation);
            }
        })
    };

//postRedirect('mysite.com/form', {arg1: 'value1', arg2: 'value2'});

  const getAPIdata = () => {
    return APIareas;
  }
  const getCurrentProperty = () => {
    return currentProperty;
  }

  return {
    init,
    showQuoteForm,
    submitEmail,
    openBillingModal,
    openReserveModal,
    reserve,
    postRedirect,

    toggleMenu,
    scrollDown,
    scrollTop,
    toggleConceptView,

    getAPIdata,
    getCurrentProperty,
  }

})();

window.Interactive = Interactive;

if (module && module.hot) module.hot.accept();