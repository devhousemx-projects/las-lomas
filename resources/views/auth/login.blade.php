<!DOCTYPE html>
@extends('layouts.admin')
@section('css-include')
  <!-- iCheck -->
  <link rel="stylesheet" href="{{asset('admin/AdminLTE/plugins/iCheck/square/blue.css')}}">
@endsection

@section('content')
	<div class="login-box">
	  <div class="login-logo">
	    <a href="{{ route('home') }}">
	    	<img src="{{ asset('assets/img/logo_est.svg') }}" alt="" width="150">
	    </a>
	  </div>
	  <!-- /.login-logo -->
	  <div class="login-box-body">
	    <p class="login-box-msg">Enter your credentials</p>

		@if (count($errors) > 0)
			<div class="alert alert-danger">
				We found some errors.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif

	    <form action="{{ route('login') }}" method="post">
		  <input type="hidden" name="_token" value="{{ csrf_token() }}">
	      <div class="form-group has-feedback">
	        <input type="email" name="email" class="form-control" placeholder="Email" value="{{ old('email') }}">
	        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
	      </div>
	      <div class="form-group has-feedback">
	        <input type="password" name="password" class="form-control" placeholder="Password">
	        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
	      </div>
	      <div class="row">
	        <div class="col-xs-8">
	          <div class="checkbox icheck">
	            <label>
	              <input name="remember" type="checkbox"> Remember me
	            </label>
	          </div>
	        </div>
	        <!-- /.col -->
	        <div class="col-xs-4">
	          <button type="submit" class="btn btn-primary btn-block btn-flat">Log in</button>
	        </div>
	        <!-- /.col -->
	      </div>
	    </form>

	    <!-- <a href="{{ route('password.request') }}">Olvidé mi contraseña</a><br> -->
	<!-- <a href="register.html" class="text-center">Register a new membership</a> -->

	  </div>
	  <!-- /.login-box-body -->
	</div>
	<!-- /.login-box -->
@endsection

@section('js-include')

	<!-- iCheck -->
	<script src="{{asset('admin/AdminLTE/plugins/iCheck/icheck.min.js')}}"></script>

	<script>
	  $(function () {
	    $('input').iCheck({
	      checkboxClass: 'icheckbox_square-blue',
	      radioClass: 'iradio_square-blue',
	      increaseArea: '20%' /* optional */
	    });
	  });
	</script>
@endsection
