<!DOCTYPE html>
<html lang="es-MX">
<head>
	<meta charset="UTF-8">
	<title>Formulario de Contacto Simulador</title>
</head>
<body>
	<h3>Contacto interesado en área {{ $area }} del Nivel {{ $level }}</h3>
	<ul>
		<li>Nombre: {{ $name }}</li>
		<li>Email: {{ $email }}</li>
		<li>Teléfono: {{ $phone }}</li>
        @if(!empty($comments) && $comments !== '')
		<li>Comentarios:  {{ $comments }}</li>
        @endif
	</ul>
</body>
</html>