<!DOCTYPE html>
<html lang="es-MX">
<head>
	<meta charset="UTF-8">
	<title>Formulario de Contacto</title>
</head>
<body>
	<h3>Nuevo mensaje de formulario web <b>Nowus</b></h3>
	<ul>
		<li>Nombre: {{ $name }}</li>
		<li>Email: {{ $email }}</li>
		<li>Mensaje:  {{ $text }}</li>
	</ul>
</body>
</html>