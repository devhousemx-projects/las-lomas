@extends('layouts.master')
@section('content')
<section id="disponibilidad" class="availability-module">
    <div id="map" class="map">
            <!--<div class="control-indicator">
                <img src="{{ asset('images/simulator/ico-north.svg') }}" alt="">
            </div>-->
            <div class="notations">
                <!--<div class="notations__title">
                    <img src="images/ico-marker.png" alt="">
                </div>-->
                <span class="notations__legend">Elige tu lote</span>
                <ul class="notations__list">
                    <li class="notation">
                        <span class="notation__color notation__color__available"></span>
                        <span class="notation__label">Disponible</span>
                    </li>
                    <li class="notation">
                        <span class="notation__color notation__color__sold"></span>
                        <span class="notation__label">Vendido</span>
                    </li>
                    <li class="notation">
                        <span class="notation__color notation__color__reserved"></span>
                        <span class="notation__label">Apartado</span>
                    </li>
                    <li class="notation">
                        <span class="notation__color notation__color__unavailable"></span>
                        <span class="notation__label">No disponible</span>
                    </li>
                </ul>
            </div>
            <div id="popup-quickview" class="popup">
                <div class="area-resume">
                    <div id="popup-content"></div>
                    <div class="area-resume__name">LOTE <span class="display-area">45</span></div>
                    <ul class="area-resume__details">
                        <li>
                            <span class="area-resume__label">Superficie total:</span>
                            <span class="area-resume__value"><span class="display-surface">379.69</span> m<sup>2</sup></span>
                        </li>
                        <li>
                            <span class="area-resume__label">Manzana:</span>
                            <span class="area-resume__value"><span class="display-block">11</span></span>
                        </li>
                        <li>
                            <span class="area-resume__label">Metros frente:</span>
                            <span class="area-resume__value"><span class="display-frontSize">21.7</span> m</span>
                        </li>
                        <li>
                            <span class="area-resume__label">Fecha de entrega:</span>
                            <span class="area-resume__value"><span class="display-delivery">Marzo 2021</span></span>
                        </li>
                        <li>
                            <span class="area-resume__label">Clave catastral:</span><br />
                            <span class="display-reference">046039</span>
                        </li>
                    </ul>
                    <button id="btn-showBilling" onclick="window.Interactive.openReserveModal()" class="btn area-resume__button">Me interesa</button>
                    <div id="require-quote" class="area-resume__require">
                        <input id="require-quote-email" type="email" name="email" placeholder="Ingresar email" autocomplete="off">
                        <small class="error-legend">Dirección de email no válida</small>
                        <small>* Al enviar acepta nuestra <a href="#">política de privacidad</a></small>
                        <button class="btn area-resume__button" onclick="window.Interactive.submitEmail()">Consultar</button>
                    </div>
                </div>
            </div>
            <div id="modal-billing" class="modal">
                <div class="modal-content">
                    <button class="btn btn-close" onclick="$('#modal-billing').fadeOut();"><span class="icon-close"></span></button>
                    <div class="container">

                        <div class="row">
                            <div class="col-6 col-md-3">
                                <img src="images/lg-laslomas.svg" alt="">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="area-resume">
                                    <div class="area-resume__name">LOTE <span class="display-area">45</span></div>
                                    <ul class="area-resume__details">
                                        <li>
                                            <span class="area-resume__label">Superficie total:</span>
                                            <span class="area-resume__value"><span class="display-surface">379.69</span> m<sup>2</sup></span>
                                        </li>
                                        <li>
                                            <span class="area-resume__label">Manzana:</span>
                                            <span class="area-resume__value"><span class="display-block">11</span></span>
                                        </li>
                                        <li>
                                            <span class="area-resume__label">Metros frente:</span>
                                            <span class="area-resume__value"><span class="display-frontSize">21.7</span> m</span>
                                        </li>
                                        <li>
                                            <span class="area-resume__label">Clave catastral:</span>
                                            <span class="area-resume__value"><span class="display-reference">046039</span></span>
                                        </li>
                                        <li>
                                            <span class="area-resume__label">Fecha de entrega:</span>
                                            <span class="area-resume__value"><span class="display-delivery">Marzo 2021</span></span>
                                        </li>
                                    </ul>
                                    <div class="area-resume__price">
                                        <span class="area-resume__price-label">Precio de lista</span>
                                        <span class="area-resume__price-value">$<span class="display-listPrice">4,565.00</span> /m<sup>2</sup></span>
                                    </div>
                                    <div class="area-resume__price">
                                        <span class="area-resume__price-label">Precio total</span>
                                        <span class="area-resume__price-value">$<span class="display-price">1,733,284.85</span></span>
                                    </div>
                                    <button class="btn area-resume__button area-resume__button__info" onclick="window.Interactive.openReserveModal()">Estoy interesado</button>
                                    <button class="btn area-resume__button area-resume__button__payment" onclick="window.Interactive.postRedirect()">Aparta tu lote</button>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="tbl-billing-wp">
                                    <table id="tbl-billing" class="table">
                                        <thead>
                                            <tr>
                                                <th>Plan</th>
                                                <th>Descuento</th>
                                                <th>Precio Total m<sup>2</sup></th>
                                                <th>Enganche</th>
                                                <th>Promoción Vigente</th>
                                                <th>Precio Total</th>
                                                <th>Enganche a los 7 días</th>
                                                <th>Número de meses</th>
                                                <th>Monto de mensualidades</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Contado</td>
                                                <td><span class="display-dto_contado"></span></td>
                                                <td><span class="display-precio_m2_contado"></span></td>
                                                <td><span class="display-enganche_contado"></span></td>
                                                <td><span class="display-promocion_vigente_contado"></span></td>
                                                <td><span class="display-precio_contado"></span></td>
                                                <td><span class="display-enganche_mes_contado"></span></td>
                                                <td><span class="display-meses_contado">0</span></td>
                                                <td><span class="display-mensualidad_contado"></span></td>
                                            </tr>
                                            <tr>
                                                <td>6 meses</td>
                                                <td><span class="display-dto_6meses"></span></td>
                                                <td><span class="display-precio_m2_6meses"></span></td>
                                                <td><span class="display-enganche_6meses"></span></td>
                                                <td><span class="display-promocion_vigente_6meses"></span></td>
                                                <td><span class="display-precio_6meses"></span></td>
                                                <td><span class="display-enganche_mes_6meses"></span></td>
                                                <td><span class="display-meses_6meses">6</span></td>
                                                <td><span class="display-mensualidad_6meses"></span></td>
                                            </tr>
                                            <tr>
                                                <td>12 meses</td>
                                                <td><span class="display-dto_12meses"></span></td>
                                                <td><span class="display-precio_m2_12meses"></span></td>
                                                <td><span class="display-enganche_12meses"></span></td>
                                                <td><span class="display-promocion_vigente_12meses"></span></td>
                                                <td><span class="display-precio_12meses"></span></td>
                                                <td><span class="display-enganche_mes_12meses"></span></td>
                                                <td><span class="display-meses_12meses">12</span></td>
                                                <td><span class="display-mensualidad_12meses"></span></td>
                                            </tr>
                                            <tr>
                                                <td>18 meses</td>
                                                <td><span class="display-dto_18meses"></span></td>
                                                <td><span class="display-precio_m2_18meses"></span></td>
                                                <td><span class="display-enganche_18meses"></span></td>
                                                <td><span class="display-promocion_vigente_18meses"></span></td>
                                                <td><span class="display-precio_18meses"></span></td>
                                                <td><span class="display-enganche_mes_18meses"></span></td>
                                                <td><span class="display-meses_18meses">18</span></td>
                                                <td><span class="display-mensualidad_18meses"></span></td>
                                            </tr>
                                            <tr>
                                                <td>24 meses</td>
                                                <td><span class="display-dto_24meses"></span></td>
                                                <td><span class="display-precio_m2_24meses"></span></td>
                                                <td><span class="display-enganche_24meses"></span></td>
                                                <td><span class="display-promocion_vigente_24meses"></span></td>
                                                <td><span class="display-precio_24meses"></span></td>
                                                <td><span class="display-enganche_mes_24meses"></span></td>
                                                <td><span class="display-meses_24meses">24</span></td>
                                                <td><span class="display-mensualidad_24meses"></span></td>
                                            </tr>
                                            <tr>
                                                <td>36 meses</td>
                                                <td><span class="display-dto_36meses"></span></td>
                                                <td><span class="display-precio_m2_36meses"></span></td>
                                                <td><span class="display-enganche_36meses"></span></td>
                                                <td><span class="display-promocion_vigente_36meses"></span></td>
                                                <td><span class="display-precio_36meses"></span></td>
                                                <td><span class="display-enganche_mes_36meses"></span></td>
                                                <td><span class="display-meses_36meses">36</span></td>
                                                <td><span class="display-mensualidad_36meses"></span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <p>
                                    <small>* Estos costos son sólo referenciales. Para mayor información comunicarse con el departamento de ventas</small>
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div id="modal-reserve" class="modal">
                <div class="modal-content">
                    <button class="btn btn-close" onclick="$('#modal-reserve').fadeOut();"><span class="icon-close"></span></button>
                    <div class="container">

                        <div class="row">
                            <div class="col">
                                <div id="appointment-form" class="appointment-form">
                                    <div class="appointment-form__title">
                                        <p>Estoy interesado en:</p>
                                        <h2>LOTE <span class="display-area"></span></h2>
                                        <p>Manzana <span class="display-block"></span></p>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="name" placeholder="Nombre">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="phone" placeholder="Teléfono">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="comment" placeholder="Comentarios (opcional)">
                                    </div>
                                    <div class="form-actions">
                                        <button class="btn btn-primary" onclick="window.Interactive.reserve()">Enviar</button>
                                    </div>
                                </div>
                                <div id="appointment-response" class="appointment-response">
                                    <h2>GRACIAS <small>POR SU INTERÉS</small></h2>
                                    <p>Uno de nuestros asesores se pondrá en contacto con usted.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>
@endsection
@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<!--<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBvSs172REoJDfGBI9XEWehmXlydhdcKEY"></script>-->
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script src="{{ asset('js/app.js') }}?2107162016"></script>
<script>
    //window.Web.init();
    window.Interactive.init();
    /*$(".btn-video-modal").on('click',function () {
        var theModal = $(this).data("bs-target"),
            videoSRC = $(this).data("video"),
            videoSRCauto = videoSRC + "?modestbranding=1&rel=0&controls=1&showinfo=0&html5=1&autoplay=1&loop=1";
            console.log(videoSRC);
            console.log($(theModal + ' iframe'));
        $(theModal + ' iframe').attr('src', videoSRCauto);
        $(theModal).on('hidden.bs.modal', function () {
            $(theModal + ' iframe').attr('src', videoSRC);
        });
    });
    $('.frm-contact').on('submit',function(e){
        e.preventDefault();
        let form = $(this);
        let action = $(this).attr('action');
        $.ajax({
            type: "POST",
            url: action,
            data: form.serialize(),
            dataType: 'json',
            success: function(){
                console.log('form ready!');
            },
            error: function(error){
                let value = form.find('button').html();
                form.find('button').html('Hubo un error al enviar, reintenta nuevamente.');
                setTimeout(function(){
                    form.find('button').html(value);
                },3000)
                console.log('Error',error);
            }
        });
    })*/
</script>
@endsection