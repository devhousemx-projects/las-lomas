@extends('admin.layout.app')

@section('content')

<section class="content-header">
	<h1>
		Areas
	</h1>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-aqua">
				<div class="inner">
					<h3>{{ $total }}</h3>

					<p>Areas Totales</p>
				</div>
				<div class="icon">
					<i class="fa fa-building"></i>
				</div>
			</div>
		</div>
		<!-- ./col -->
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-red">
				<div class="inner">
					<h3>{{ round( $sold / ($total  / 100), 2 ) }} <sup style="font-size: 20px">%</sup></h3>

					<p>{{ $sold }} Espacios Vendidos</p>
				</div>
				<div class="icon">
					<i class="fa fa-tag"></i>
				</div>
				<!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
			</div>
		</div>
		<!-- ./col -->
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-green">
				<div class="inner">
					<h3>{{ round( $available / ($total  / 100), 2 ) }} <sup style="font-size: 20px">%</sup></h3>

				<p>{{ $available }} Disponibles</p>
				</div>
				<div class="icon">
					<i class="fa fa-check-circle"></i>
				</div>
			</div>
		</div>
		<!-- ./col -->
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-yellow">
				<div class="inner">
					<h3>{{ round( $separated / ($total  / 100), 2 ) }} <sup style="font-size: 20px">%</sup></h3>

					<p>{{ $separated }} Espacios Reservados</p>
				</div>
				<div class="icon">
					<i class="ion ion-key"></i>
				</div>
			</div>
		</div>
		<!-- ./col -->
	</div>
	<!-- /.row -->
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Configuración de Instagram Feed</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<p>Para mostrar las fotografías de tu página de Instagram en la página web, se necesita conceder acceso a la página de Facebook enlazada al perfil de Instragram a mostrar.</p>
					@if($instagram->hasUserAccessToken)
						<div class="form-group">
							<label for="">Usuario:</label>
							<input type="text" value="{{ $instagramUser['username']; }}">
						</div>
						<div class="form-group">
							<label for="">Instagram ID:</label>
							<input type="text" value="{{ $instagramUser['id']; }}">
						</div>
						<div class="form-group">
							<label for="">Media Count:</label>
							<input type="text" value="{{ $instagramUser['media_count']; }}">
						</div>
						<div class="form-group">
							<label for="">Account type:</label>
							<input type="text" value="{{ $instagramUser['account_type']; }}">
						</div>
						<pre>
							<?php print_r( $instagramUser ); ?>
						</pre>
					@endif
                    <p>
						<a href="{{ $instagram->authorizationUrl }}" class="btn btn-primary"><i class="fa fa-lock"></i> Autorizar acceso</a>
                    </p>
                </div>
			</div>
			<!-- /.box -->
		</div>
	</div>
</section>
@stop

@section('css-include')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('admin/AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('js-include')
<!-- DataTables -->
<script src="{{asset('admin/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('admin/AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>
	(function() {
		$('#table-areas thead tr').clone(true).appendTo( '#table-areas thead' );
		$('#table-areas thead tr:eq(1) th.filter').each( function (i) {
			var title = $(this).text();
			$(this).html( '<input type="text" placeholder="Búsqueda por '+title+'" />' );

			$( 'input', this ).on( 'keyup change', function () {
				if ( table.column(i).search() !== this.value ) {
					table
					.column(i)
					.search( "^" + $(this).val() + "$", true, false, true )
					.draw();
				}
			} );
		} );

		var table = $('#table-areas').DataTable( {
			orderCellsTop: true,
			fixedHeader: true,
			order: [[1, 'asc']]
		} );
	})();
</script>
@stop
