@extends('admin.layout.app')

@section('content')

<section class="content-header">
	<h1>
		Areas
		<a href="{{ route('admin.blog.create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Nuevo artículo</a>
	</h1>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			@if (\Session::has('success'))
			<div class="alert alert-success">
				{{ \Session::get('success') }}.
			</div>
			@endif
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Listado de artículos</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body table-responsive">
					<table id="table-areas" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>ID</th>
								<th>Imagen</th>
								<th>Título</th>
								<th>Publicado el</th>
								<th>Estado</th>
								<th>Opciones</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($articles as $article)
							<tr id="areas-{{ $article->id }}">
								<td>{{ $article->id }}</td>
								<td><img src="{{ asset('uploads/articles/'.$article->image) }}" width="65" height="65" /></td>
								<td>{{ $article->title }}</td>
								<td>{{ $article->created_at->format('Y-m-d') }}</td>
								<td>
									@if (!$article->status)
									<span class="label label-default">Borrador</span>
									@else
									<span class="label label-success">Publicado</span>
									@endif
								</td>
								<td>
									<a title="Editar" class="btn btn-primary" href="{{ route('admin.blog.edit', ['id' => $article->id]) }}">
										<i class="fa fa-pencil"></i>
									</a>
									<a href="{{ route('admin.blog.delete', ['id' => $article->id]) }}" class="btn btn-default btn-confirm" data-reference="{{ $article->title }}"><i class="fa fa-trash"></i></a>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
				<div class="box-footer clearfix text-center hidden">
					{{-- {!! $menus->render() !!} --}}
				</div>
				<!-- /.box -->
			</div>

			<!-- /.box -->
		</div>
	</div>
</section>
@stop

@section('css-include')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('admin/AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<!-- Swal -->
<link rel="stylesheet" href="{{ asset('vendor/sweetalert/dist/sweetalert.css') }}">
@endsection

@section('js-include')
<!-- DataTables -->
<script src="{{asset('admin/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('admin/AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>
	(function() {
		$('#table-areas thead tr').clone(true).appendTo( '#table-areas thead' );
		$('#table-areas thead tr:eq(1) th.filter').each( function (i) {
			var title = $(this).text();
			$(this).html( '<input type="text" placeholder="Búsqueda por '+title+'" />' );

			$( 'input', this ).on( 'keyup change', function () {
				if ( table.column(i).search() !== this.value ) {
					table
					.column(i)
					.search( "^" + $(this).val() + "$", true, false, true )
					.draw();
				}
			} );
		} );

		var table = $('#table-areas').DataTable( {
			orderCellsTop: true,
			fixedHeader: true,
			order: [[0, 'desc']]
		} );
	})();
</script>
<script src="{{ asset('vendor/sweetalert/dist/sweetalert.min.js') }}"></script>
<script>
	$(document).ready(function() {
		$.ajaxSetup({
			headers: {
				"X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
			}
		});
		$('.btn-confirm').on('click',function(e){
			e.preventDefault();
			let reference = $(this).attr('data-reference') || '';
			let buttonUrl = $(this).attr('href') || '';
			swal({
				title: "¿Estas seguro?",
				text: `Se eliminará de manera permanente ${reference.length ? '"'+reference+'"' : ''}.`,
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Sí, eliminar.",
				closeOnConfirm: false
			}, function (isConfirm) {
				if (!isConfirm) return;
				$.ajax({
					url: buttonUrl,
					type: "DELETE",
					dataType: "json"
				})
				.done(function(result){
					if (result.success) {
						if (
							result.success.message != undefined &&
							result.success.message.length > 0
						) {
							swal(
								{
									title: result.success.title,
									text: result.success.message,
									type: "success",
									showConfirmButton: true
								},
								function() {
								}
							);
						}

						var timeout = $(this).attr("data-timeout");

						if (timeout == null || timeout == undefined) timeout = 3000;

						setTimeout(function() {
							// Mantenemos
							if (result.success.url == "keep") {
								swal.close();

								if (result.success.download) {
									window.open(result.success.download, "blank");
								}
							}
							// Recargamos la página
							else if (result.success.url == "reload") {
								window.location.reload();
							}
							// Dirección
							else {
								window.location.href = result.success.url;
							}
						}, timeout);

						if (result.success.callback) {
							eval(result.success.callback);
						}
						//grecaptcha.reset();
					} else if (result.error) {
						swal({
							title: "¡Oops!",
							text: result.error.message,
							type: "error",
							showConfirmButton: true
						});
					} else {
						swal.close();
						$.each(result.messages, function(i, el) {
							$(".input-" + i + " .input-error", form).html(
								result.messages[i]
							);
							$(
								'input[name="' +
									i +
									'"], select[name="' +
									i +
									'"], textarea[name="' +
									i +
									'"]',
								form
							).addClass("form-error");
							console.log(".input-" + i + " .text-danger");
						});
					}
				});
			});
		});
	});
</script>
@stop
