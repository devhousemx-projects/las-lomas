@extends('admin.layout.app')

@section('content')

<section class="content-header">
    <h1>
        @if(isset($article))
            Editar artículo - {{ $article->title }}
        @else
            Crear artículo
        @endif
	</h1>
</section>

<!-- Main content -->
<section class="content">
    <!-- left column -->
    @if(isset($article))
        <form action="{{route('admin.blog.update', ['id' => $article->id]) }}" method="POST" class="form-ajax" enctype="multipart/form-data">
    @else
        <form action="{{route('admin.blog.store') }}" method="POST" class="form-ajax" enctype="multipart/form-data">
    @endif
		<div class="row">
			<div class="col-md-8">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Información del artículo</h3>
					</div>
					<!-- form start -->
					<div class="box-body">
						<div class="box-actions">
							<div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="">Título</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-header"></i></span>
                                            <input type="text" class="form-control input-lg limit-length" name="title" data-limitHelpText=".title_counter" maxlength="150" value="{{ $article->title ?? '' }}" placeholder="Título del artículo" required>
                                            <input type="hidden" name="slug" class="form-control" id="slug" value="{{ $article->slug ?? '' }}" placeholder="Slug del artículo"readonly>
                                        </div>
                                        <small><span class="title_counter">150</span> caraceres restantes</small>
                                        <div style="margin-top:5px;">
                                            <small>Url: <a href="{{ isset($article) ? 'http://ellucero.mx/blog/'.$article->slug : '#' }}" class="article-link" target="_blank">{{ isset($article) ? 'http://ellucero.mx/blog/'.$article->slug : '#' }}</a> 
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="area">Extracto</label>
                                        <textarea name="excerpt" class="form-control limit-length" data-limitHelpText=".excerpt_counter" rows="4" maxlength="250" id="excerpt" placeholder="Introducción del artículo" required>{{ $article->excerpt ?? ''}}</textarea>
                                        <small><span class="excerpt_counter">250</span> caraceres restantes</small>
                                    </div>
                                    <div class="form-group input-slug">
                                        <div class="text-danger"></div>
                                    </div>
                                </div>
							</div>
							<div class="row">
                                <div class="col-sm-12">
                                    <label for="status">Contenido:</label>
                                    <div class="text-editor"></div>
                                    <textarea name="content" id="article-content" cols="30" rows="10" style="display:none;">{{ $article->content ?? '' }}</textarea>
                                    
                                </div>
							</div>
						</div>
					</div>
				</div>
            </div>
            <div class="col-md-4">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Información del artículo</h3>
					</div>
					<!-- form start -->
					<div class="box-body">
						<div class="box-actions">
							<div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="status">Estado:</label>
                                        @if(isset($article))
                                            @if ($article->status == 1)
                                                <span class="label label-success">Publicado</span>
                                            @else
                                                <span class="label label-default">Borrador</span>
                                            @endif
                                        @endif
                                        <select name="status" id="status" class="form-control">
                                            <option {{ isset($article) && $article->status == 1 ? 'selected' : '' }} value="1">Publicado</option>
                                            <option {{ isset($article) && $article->status == 0 ? 'selected' : '' }} value="0">Desactivado</option>
                                        </select>
                                    </div>
                                    <div class="form-group input-status">
                                        <div class="text-danger"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="status">Fecha:</label>
                                        <input type="date" class="form-control" name="created_at" value="{{ isset($article) ? $article->created_at->format('Y-m-d') : date('Y-m-d') }}" pattern="\d{1,2}/\d{1,2}/\d{4}" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="box-body">
						<div class="box-actions">
							<div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="file-cover">Portada (en blog):</label>
                                        <small>Recomendado 960px x 960px</small>
                                        @if(isset($article))
                                            <img src="{{ asset('uploads/articles/'.$article->image) }}" style="max-width: 100%" />
                                        @endif
                                        <input type="file" name="file-thumb" class="form-control" accept="image/*">
                                    </div>
                                    <div class="form-group">
                                        <label for="file-cover">Portada (en artículo):</label>
                                        <small>Recomendado 1920px x 445px</small>
                                        @if(isset($article))
                                            <img src="{{ asset('uploads/articles/'.$article->imageHead) }}" style="max-width: 100%" />
                                        @endif
                                        <input type="file" name="file-cover" class="form-control" accept="image/*">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="{{ route('admin.blog') }}" class="btn btn-default">Cancelar</a>
                        <div class="pull-right">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </div>
            </div>
		</div>
		<!-- /.box -->
	</form>
</section>
@stop

@section('css-include')
    <link href="//cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('vendor/sweetalert/dist/sweetalert.css') }}">
    <style>
        .ql-editor{min-height:300px;}
    </style>
@endsection
  
@section('js-include')
    <script src="//cdn.quilljs.com/1.3.6/quill.min.js"></script>
    <script>
        var toolbarOptions = [
            [{ 'header': 2 }],
            ['bold', 'italic', 'underline'], 
            [{ 'list': 'ordered'}, { 'list': 'bullet' }],
            ['link', 'image'],
            ['clean'] 
        ];
        var quill = new Quill('.text-editor', {
            theme: 'snow',
            modules: {
                toolbar: toolbarOptions
            }
        });

        quill.on('text-change', function(delta, oldDelta, source) {
            $('#article-content').text(quill.root.innerHTML.trim());

        });

        @if(isset($article))
            quill.clipboard.dangerouslyPasteHTML('{!! html_entity_decode($article->content) !!}');
            //quill.setText('{!! html_entity_decode($article->content) !!}');
        @endif

        var slug = function(str) {
            str = str.replace(/^\s+|\s+$/g, ''); // trim
            str = str.toLowerCase();

            // remove accents, swap ñ for n, etc
            var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
            var to   = "aaaaaeeeeeiiiiooooouuuunc------";
            for (var i=0, l=from.length ; i<l ; i++) {
                str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
            }

            str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
                .replace(/\s+/g, '-') // collapse whitespace and replace by -
                .replace(/-+/g, '-'); // collapse dashes

            return str;
        };
        $('input[name="title"]').on('keyup',function(){
            let title = $('input[name="title"]').val();
            let articleSlug = slug(title);
            let articleLink = "http://ellucero.mx/blog/"+articleSlug;
            $('input[name="slug"]').val(articleSlug);
            $('.article-link').attr("href",articleLink).html(articleLink);
        })

        $('.limit-length').on('keyup',function(){
            let content = $(this).val() || 0;
            let contentLength = content.length || 0;
            let maxLength = $(this).attr('maxlength') || 0;
            let available = maxLength - contentLength;
            let helpText = $($(this).attr('data-limitHelpText'));
            $(helpText).html(available);
        });
        $('.limit-length').trigger('keyup');
        
    </script>
	<script src="{{ asset('vendor/sweetalert/dist/sweetalert.min.js') }}"></script>
	<script src="{{ asset('js/ajax.js') }}"></script>
@endsection