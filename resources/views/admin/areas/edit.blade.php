@extends('admin.layout.app')

@section('content')

<section class="content-header">
	<h1>
		Editar área - {{ $area->id }}
	</h1>
</section>

<!-- Main content -->
<section class="content">
	<!-- left column -->
	<form action="{{route('admin.areas.update', ['id' => $area->id]) }}" method="POST" class="form-ajax">
		<div class="row">
			<div class="col-md-6">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Información del área</h3>
					</div>
					<!-- form start -->
					<div class="box-body">
						<div class="box-actions">
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label for="level">Nivel</label>
										<small>{{ $area->level->level }}</small>
										<input type="text" name="level" class="form-control" id="level" value="{{ $area->level->level }}" placeholder="Nivel en el que se encuentra el área" disabled readonly>
									</div>
									<div class="form-group input-level">
										<div class="text-danger"></div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label for="area">Área</label>

										<input type="text" name="area" class="form-control" id="area" value="{{ $area->area }}" placeholder="Identificador del area" disabled readonly>
									</div>
									<div class="form-group input-area">
										<div class="text-danger"></div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label for="status">Estado:</label>
										@if ($area->status == 1)
											<span class="label label-success">Disponible</span>
										@elseif( $area->status == 2 )
											<span class="label label-warning">Reservado</span>
										@else
											<span class="label label-danger">Vendido</span>
										@endif
										<select name="status" id="status" class="form-control">
											<option {{ $area->status == 1 ? 'selected' : '' }} value="1">Disponible</option>
											<option {{ $area->status == 2 ? 'selected' : '' }} value="2">Reservado</option>
											<option {{ $area->status == 0 ? 'selected' : '' }} value="0">Vendido</option>
										</select>
									</div>
									<div class="form-group input-status">
										<div class="text-danger"></div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label for="">Precio</label>
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-dollar"></i></span>
											<input type="number" step="0.01" class="form-control" name="price" value="{{ $area->price }}" placeholder="Precio">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="box-footer">
			<a href="{{ route('admin.areas') }}" class="btn btn-danger">Cancelar</a>
			<div class="pull-right">
				<button type="submit" class="btn btn-success">Guardar</button>
			</div>
		</div>
		<!-- /.box -->
	</form>
</section>
@stop

@section('css-include')
  <link rel="stylesheet" href="{{ asset('admin/bower_components/sweetalert/dist/sweetalert.css') }}">
@endsection

@section('js-include')
	<script src="{{ asset('admin/bower_components/sweetalert/dist/sweetalert.min.js') }}"></script>
	<script src="{{ asset('js/ajax.js') }}"></script>
@endsection