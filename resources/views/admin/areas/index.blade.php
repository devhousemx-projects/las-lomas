@extends('admin.layout.app')

@section('content')

<section class="content-header">
	<h1>
		Lotes
	</h1>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-aqua">
				<div class="inner">
					<h3>{{ $total }}</h3>

					<p>Total lotes</p>
				</div>
				<div class="icon">
					<i class="fa fa-building"></i>
				</div>
			</div>
		</div>
		<!-- ./col -->
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-red">
				<div class="inner">
					<h3>{{ $total > 0 ? round( $sold / ($total  / 100), 2 ) : 0 }} <sup style="font-size: 20px">%</sup></h3>

					<p>{{ $sold }} vendidos</p>
				</div>
				<div class="icon">
					<i class="fa fa-tag"></i>
				</div>
				<!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
			</div>
		</div>
		<!-- ./col -->
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-green">
				<div class="inner">
					<h3>{{ $total > 0 ? round( $available / ($total  / 100), 2 ) : 0 }} <sup style="font-size: 20px">%</sup></h3>

				<p>{{ $available }} disponibles</p>
				</div>
				<div class="icon">
					<i class="fa fa-check-circle"></i>
				</div>
			</div>
		</div>
		<!-- ./col -->
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-yellow">
				<div class="inner">
					<h3>{{ $total > 0 ? round( $separated / ($total  / 100), 2 ) : 0 }} <sup style="font-size: 20px">%</sup></h3>

					<p>{{ $separated }} reservados</p>
				</div>
				<div class="icon">
					<i class="ion ion-key"></i>
				</div>
			</div>
		</div>
		<!-- ./col -->
	</div>
	<!-- /.row -->
	<div class="row">
		<div class="col-xs-12">
			@if (\Session::has('success'))
			<div class="alert alert-success">
				{{ \Session::get('success') }}.
			</div>
			@endif
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Lista de lotes</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body table-responsive">
					<table id="table-areas" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>ID</th>
								<th>Complejo</th>
								<th>Manzana</th>
								<th>Lote</th>
								<th>Estado</th>
								<th>Acciones</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($areas as $area)
							<tr id="areas-{{ $area->id }}">
								<td>{{ $area->id }}</td>
								<td>{{ $area->level->building->name }} <?php echo (!$area->level->building->active) ? '<br /><small><span class="label label-danger">No Disponible</span></small>' : ''; ?></td>
								<td>{{ $area->manzana }}</td>
								<td>{{ $area->area }}</td>
								<td>
									@if (!$area->status)
									<span class="label label-danger">Vendido</span>
									@elseif( $area->status == 2 )
									<span class="label label-warning">Reservado</span>
									@else
									<span class="label label-success">Disponible</span>
									@endif
								</td>
								<td>
									<!--<a title="Editar" class="btn btn-primary" href="{{ route('admin.areas.edit', ['id' => $area->id]) }}">
										<i class="fa fa-pencil"></i>
									</a>-->
									<a title="Marcar como Disponible" href="{{ route('admin.areas.available', ['id' => $area->id]) }}" class="btn btn-default">
										<i class="fa fa-check text-success"></i>
									</a>
									<a title="Marcar como Reservado" class="btn btn-default" href="{{ route('admin.areas.separated', ['id' => $area->id]) }}">
										<i class="fa fa-lock text-warning"></i>
									</a>
									<a title="Marcar como Vendido" class="btn btn-default" href="{{ route('admin.areas.sold', ['id' => $area->id]) }}">
										<i class="fa fa-tag text-danger"></i>
									</a>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
				<div class="box-footer clearfix text-center hidden">
					{{-- {!! $menus->render() !!} --}}
				</div>
				<!-- /.box -->
			</div>

			<!-- /.box -->
		</div>
	</div>
</section>
@stop

@section('css-include')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('admin/AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('js-include')
<!-- DataTables -->
<script src="{{asset('admin/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('admin/AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>
	(function() {
		$('#table-areas thead tr').clone(true).appendTo( '#table-areas thead' );
		$('#table-areas thead tr:eq(1) th.filter').each( function (i) {
			var title = $(this).text();
			$(this).html( '<input type="text" placeholder="Búsqueda por '+title+'" />' );

			$( 'input', this ).on( 'keyup change', function () {
				if ( table.column(i).search() !== this.value ) {
					table
					.column(i)
					.search( "^" + $(this).val() + "$", true, false, true )
					.draw();
				}
			} );
		} );

		var table = $('#table-areas').DataTable( {
			orderCellsTop: true,
			fixedHeader: true,
			order: [[1, 'asc']]
		} );
	})();
</script>
@stop
