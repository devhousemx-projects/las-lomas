@extends('layouts.master')
@section('content')
<section class="home" id="home">
    <div class="home-cover">
        <div id="home-video-cover" class="video-cover" style="background-image: url(images/home.jpg)">
            <video class="video-cover--video video-cover--video--static" autoplay loop autobuffer playsinline muted="muted" src="{{ asset('video/las-lomas-intro.mp4') }}">
                <source src="{{ asset('video/las-lomas-intro.mp4') }}" type="video/mp4">
            </video>
            <!--<button class="video-cover--button btn-play"><img class="w-20" src="{{ asset('images/ico-play.svg') }}" alt="Play"></button>-->
        </div>
        <div class="caption">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        Una realidad a la altura de tus sueños.<br />
                        <a href="{{ route('salesModule') }}" class="btn btn-secondary">VER DISPONIBILIDAD</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="nosotros" class="intro">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="intro-video">
                    <div class="video-cover" style="background-image: url(images/img-cover-intro.gif)">
                        <button class="video-cover--button btn-play" data-bs-toggle="modal" data-bs-target="#videoModal"><img class="w-20" src="{{ asset('images/ico-play.svg') }}" alt="Play"></button>
                    </div>
                </div>
            </div>
            <div class="offset-md-1 col-md-5">
                <h2 class="section-title">NOSOTROS</h2>
                <p>
                    Un núcleo inmobiliario ubicado en un impactante escenario que reúne paisajes naturales con el diseño orgánico y funcional de sus 4 secciones habitacionales.
                </p>
                <p>
                    Un club de golf con casa club, amenidades y área comercial, con las ventajas que brinca la cercanía, tranquilidad, servicios y exclusividad.
                </p>
            </div>
        </div>
    </div>
</section>
<section class="frame frame--top frame--bottom">
    <div class="intro-slider slider">
        <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-indicators">
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
            </div>
            <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="images/slider-intro/01.jpg" class="d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">
                <p>Contempla <span class="text-highlight-primary">el atardecer</span> de tu nuevo hogar</p>
                </div>
            </div>
            <div class="carousel-item">
                <img src="images/slider-intro/02.jpg" class="d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">
                    <p>Contempla <span class="text-highlight-primary">el atardecer</span> de tu nuevo hogar</p>
                </div>
            </div>
            <div class="carousel-item">
                <img src="images/slider-intro/03.jpg" class="d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">
                    <p>Contempla <span class="text-highlight-primary">el atardecer</span> de tu nuevo hogar</p>
                </div>
            </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
            </button>
        </div>
    </div>
</section>
<section id="master-plan" class="disponibilidad">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h2 class="section-title centered">MASTER PLAN</h2>
                <p>
                    Único Desarrollo Residencial con un exclusivo Campo de  Golf de 18 hoyos integrado, con vistas panorámicas al propio campo de golf, al valle agrícola y a la ciudad, espectacular escenario natural, amplias áreas verdes y excelente clima gracias a su elevación.
                </p>
                <p>
                    Entre nuestras cuatro secciones, se encuentran lotes con distintas características así como vistas a la ciudad, vista a los campos agrícolas y colindancia con el campo de golf.
                </p>
                <div class="mt-5 text-center">
                    <a href="{{ route('salesModule') }}" class="btn btn-primary">VER DISPONIBILIDAD</a>
                </div>
            </div>
        </div>
    </div>
    <div class="disponibilidad-separator">
        <img src="images/lg-laslomas-white.svg" alt="" class="logo" />
    </div>
</section>
<section class="servicios">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6 text-center">
                <h2 class="section-title centered">SERVICIOS</h2>
                <p>
                    Ser Residente en Las Lomas Hábitat + Golf, significa además de disfrutar de la belleza, escenografía natura  y tranquilidad del lugar, el tener servicios de primera, acceso controlado, guardias 24/7, cámaras de vigilancia  barda perimetral.
                </p>
            </div>
        </div>
        <ul class="row">
            <li class="col-6 col-md-4 col-lg-2"><span class="icon icon-controlado"></span><br />CASETA DE ACCESO CONTROLADO CON VIGILANCIA</li>
            <li class="col-6 col-md-4 col-lg-2"><span class="icon icon-vialidades"></span><br />AMPLIAS VIALIDADES</li>
            <li class="col-6 col-md-4 col-lg-2"><span class="icon icon-vigilancia"></span><br />CÁMARAS DE VIGILANCIA</li>
            <li class="col-6 col-md-4 col-lg-2"><span class="icon icon-verdes"></span><br />ÁREAS VERDES</li>
            <li class="col-6 col-md-4 col-lg-2"><span class="icon icon-perimetral"></span><br />MURO PERIMETRAL</li>
            <li class="col-6 col-md-4 col-lg-2"><span class="icon icon-comercial"></span><br />ÁREA COMERCIAL</li>
        </ul>
    </div>
    <div class="mosaic container-fluid">
        <div class="row">
            <div class="col-12 col-md-8">
                <div class="mosaic-item" style="background-image:url(images/mosaic-intro/lago_lomas.jpg)"></div>
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="mosaic-item" style="background-image:url(images/mosaic-intro/seguridad_lomas.jpg)"></div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="mosaic-item" style="background-image:url(images/mosaic-intro/caseta_lomas.jpg)"></div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="mosaic-item" style="background-image:url(images/mosaic-intro/calle_lomas.jpg)"></div>
            </div>
        </div>
    </div>
</section>
<section id="socio-club" class="club frame frame--bottom frame--bottom--primary">
    <div class="club-head">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h2 class="section-title-alt">SOCIO <span>CLUB</span></h2><button class="btn btn-secondary" data-bs-toggle="modal" data-bs-target="#clubModal">ACCIONES</button>
                </div>
            </div>
        </div>
    </div>
    <div class="slider">
        <div id="carouselExampleCaptions2" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-indicators">
                <button type="button" data-bs-target="#carouselExampleCaptions2" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                <button type="button" data-bs-target="#carouselExampleCaptions2" data-bs-slide-to="1" aria-label="Slide 2"></button>
                <button type="button" data-bs-target="#carouselExampleCaptions2" data-bs-slide-to="2" aria-label="Slide 3"></button>
            </div>
            <div class="carousel-inner">
                <div class="carousel-item active">
                <img src="images/slider-club/01.jpg" class="d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">
                    <p>Campo de golf 18 hoyos par 72</p>
                </div>
                </div>
                <div class="carousel-item">
                <img src="images/slider-club/02.jpg" class="d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">
                    <p>Campo de golf 18 hoyos par 72</p>
                </div>
                </div>
                <div class="carousel-item">
                <img src="images/slider-club/03.jpg" class="d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">
                    <p>Campo de golf 18 hoyos par 72</p>
                </div>
                </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions2" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions2" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
            </div>
    </div>
</section>
<section class="amenidades">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2 class="section-title inverted">Amenidades</h2>
                <p>
                    Se parte de la exclusividad de Las Lomas Hábitat + Golf, intégrate con tu familia al club y disfruta de todas sus amenidades como el escénico campo de golf de 18 hoyos, la deslumbrante casa club con restaurante bar y terraza. Aprovecha la vista panorámica de su alberca infinita, el gimnasio, las canchas de tenis, paddel y futbol rápido.
                </p>
                <p>
                    Tus hijos no van a querer salir de Las Lomas.
                </p>
            </div>
            <div class="list col-md-6">
                <div class="row">
                    <div class="col-6 col-md-4"><span class="icon icon-golf"></span><br /> CAMPO DE GOLF</div>
                    <div class="col-6 col-md-4"><span class="icon icon-alberca"></span><br /> ALBERCA</div>
                    <div class="col-6 col-md-4"><span class="icon icon-gym"></span><br /> GYM</div>

                    <div class="col-6 col-md-4"><span class="icon icon-tenis"></span><br /> CANCHA DE TENIS</div>
                    <div class="col-6 col-md-4"><span class="icon icon-padel"></span><br /> CANCHA DE PADEL</div>
                    <div class="col-6 col-md-4"><span class="icon icon-futbol"></span><br /> CANCHA DE FUTBOL RÁPIDO</div>

                    <div class="col-6 col-md-4"><span class="icon icon-club"></span><br /> CASA CLUB</div>
                    <div class="col-6 col-md-4"><span class="icon icon-verdes2"></span><br /> LAGOS Y ÁREAS VERDES</div>
                    <div class="col-6 col-md-4"><span class="icon icon-restaurante"></span><br /> RESTAURANTE</div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="carousel row">
            <div class="col-12 col-md-4">
                <div class="image-wp" style="background-image: url(images/amenities/01.jpg)"></div>
            </div>
            <div class="col-12 col-md-4">
                <div class="image-wp" style="background-image: url(images/amenities/02.jpg)"></div>
            </div>
            <div class="col-12 col-md-4">
                <div class="image-wp" style="background-image: url(images/amenities/03.jpg)"></div>
            </div>
        </div>
    </div>
</section>
<section class="feed">
    <div class="container">
        <div class="feed-posts row">
            
            <template id="feed-post">
                <div class="feed-post">
                    <a href="" target="_blank" class="feed-post--page-link"><span class="feed-post--logo"></span></a>
                    <div class="feed-post--card-title">
                        <a href="#" target="_blank" class="feed-post--page-link"><div class="feed-post--avatar"><span></span></div></a>
                        <div class="feed-post--meta">
                            <a href="#" target="_blank" class="feed-post--page-link"><b><span class="feed-post--page-name"></span></b></a><br />
                            <span class="feed-post--date"></span>
                        </div>
                    </div>
                    <a href="#" target="_blank" class="feed-post--post-link"><div class="feed-post--thumb"></div></a>
                    <div class="feed-post--content">
                    </div>
                </div>
            </template>

        </div>
    </div>
</section>
<section class="location">
    <div class="container-fluid">
        <div class="row">
            <div class="directory-wp col-md-6">
                <div class="headline">
                    BLVD. Paseo Loma Dorada 100<br />
                    Fracc. Las Lomas, CP 81240<br />
                    LOS MOCHIS, SINALOA MX
                </div>
                <div class="directory-intro">
                    <p>
                        Ubicado al norte en las únicas lomas habitables de la ciudad, sobre la principal salida al valle agrícola, cerca de universidades y colegios, hospitales, zonas comerciales y de entrenamiento y tan solo a 10 minutos del centro de la ciudad.
                    </p>
                    <p class="font-2x">
                        La comodidad de realizar tus actividades en tu zona.
                    </p>
                </div>
                <div class="links">
                    Ver en: <a href="https://maps.google.com/maps?ll=25.808064,-108.95388&z=15&t=m&hl=es-419&gl=MX&mapclient=embed&cid=9928420243432728214" target="_blank" class="maps-link"><img src="images/ico-googlemaps.svg" alt="Google Maps"></a>
                </div>
            </div>
            <div class="map-wp col-md-6">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d9446.31733975516!2d-108.9524768719718!3d25.806512510830615!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x86ba290c362aecfb%3A0x89c8d59bbe9ae696!2sLas%20Lomas%20Habitat%20%2B%20Golf!5e0!3m2!1ses-419!2smx!4v1646192426893!5m2!1ses-419!2smx" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            </div>
        </div>
    </div>
</section>
<section id="contacto" class="contacto">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h2 class="section-title centered">CONTACTO</h2>
                <form action="{{ route('email.send',['type' => 'contact']) }}" class="frm-contact form">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Nombre<sup>*</sup></label>
                                <input type="text" class="form-control" name="name" required/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Apellidos<sup>*</sup></label>
                                <input type="text" class="form-control" name="lastname" required/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Email<sup>*</sup></label>
                                <input type="email" class="form-control" name="email" required/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Teléfono</label>
                                <input type="text" class="form-control" name="phone" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="">Mensaje<sup>*</sup></label>
                                <textarea name="message" id="" rows="5" class="form-control" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="legends row">
                        <div class="col-md-6">
                            <small>*Obligatorio</small>
                        </div>
                        <div class="col-md-6">
                            <small>Al enviar aceptas la política de privacidad.</small>
                        </div>
                    </div>
                    <div class="actions row">
                        <div class="col-md-6">
                            <div class="g-recaptcha" data-sitekey="6Lc_sj0fAAAAAE4-q1-0yOzOC4AlbT43ovKbWGyd"></div>
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-secondary" type="submit">ENVIAR</button>
                        </div>
                    </div>
                    <div class="social">
                        <div class="row">
                            <div class="col-4">
                                <a href="" class="social-link"><span class="icon icon-facebook"></span></a>
                            </div>
                            <div class="col-4">
                                <a href="" class="social-link"><span class="icon icon-instagram"></span></a>
                            </div>
                            <div class="col-4">
                                <a href="" class="social-link"><span class="icon icon-youtube"></span></a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<section id="videoModal" class="modal fade" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <!--<button class="btn-close-modal" data-bs-dismiss="modal">CERRAR</button>-->
        <div class="modal-content modal-content--transparent">
            <div class="modal-body">
                <div class="modal-video">
                    <div class="iframe-video" data-url="https://www.youtube.com/embed/SG4ifkHeo-s?autoplay=1&loop=1&rel=0&showinfo=0&color=white">
                        <iframe src="" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="clubModal" class="modal fade" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <button class="btn-close-modal" data-bs-dismiss="modal"><span></span></button>
            <div class="modal-body">
                <div class="modal-contact">
                    <div class="modal-contact--title">
                        Estamos para servirte en el siguiente formulario de contacto o en nuestros teléfonos
                    </div>
                    <span class="modal-contact--label">
                        Por favor proporciona los siguientes datos
                    </span>
                    <div class="modal-contact--fields">
                        <form action="{{ route('email.send',['type' => 'club']) }}" class="frm-contact form">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Nombre<sup>*</sup></label>
                                        <input type="text" class="form-control" name="name" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Apellidos<sup>*</sup></label>
                                        <input type="text" class="form-control" name="lastname" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Email<sup>*</sup></label>
                                        <input type="text" class="form-control" name="email" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Teléfono</label>
                                        <input type="text" class="form-control" name="phone" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="">Mensaje<sup>*</sup></label>
                                        <textarea name="message" id="" rows="5" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="legends row">
                                <div class="col-md-6">
                                    <i><small>*Obligatorio</small></i>
                                </div>
                                <div class="col-md-6">
                                    <i><small>Al enviar aceptas la política de privacidad.</small></i>
                                </div>
                            </div>
                            <div class="actions row">
                                <div class="col-md-4">
                                </div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="g-recaptcha" data-sitekey="6Lc_sj0fAAAAAE4-q1-0yOzOC4AlbT43ovKbWGyd"></div>
                                        </div>
                                        <div class="col-md-6">
                                            <button class="btn btn-secondary" type="submit">ENVIAR</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('scripts')
<script src="{{ asset('js/fb-feed.js') }}"></script>
<script src="{{ asset('js/video-cover.js') }}"></script>
<script>
    (function() {
        const homeCover = new VideoCover({
            target: '#home-video-cover',
            autoplay: true,
            hideOnScroll: false,
        });
        const introCover = new VideoCover({
            target: '.simple-intro-video-cover'
        });
    })();
</script>
<script>

    // Video Modal
    var videoModal = document.getElementById('videoModal')
    //var myInput = document.getElementById('myInput')

    videoModal.addEventListener('show.bs.modal', function () {
        const wp = document.querySelector('.iframe-video');
        const url = wp.getAttribute('data-url');
        const iframe = wp.querySelector('iframe');
        iframe.setAttribute('src',url);
    })
    videoModal.addEventListener('hide.bs.modal', function () {
        const wp = document.querySelector('.iframe-video');
        const iframe = wp.querySelector('iframe');
        iframe.setAttribute('src','');
    })

    //
    var clubModal = document.getElementById('clubModal')
    //var myInput = document.getElementById('myInput')

    clubModal.addEventListener('shown.bs.modal', function () {
        //myInput.focus()
    })
</script>
@endsection