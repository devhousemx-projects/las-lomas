<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @yield('title')
    
    <link
      rel="stylesheet"
      href="{{ asset('plugins/bootstrap-4.3.1-dist/css/bootstrap.min.css') }}"
    />
    <link rel="stylesheet" href="{{ asset('vendor/animate.css/animate.min.css') }}" />
    <link rel="stylesheet" href="{{asset('vendor/sweetalert/dist/sweetalert.css')}}">

    @yield('css-include')

    <link
    rel="stylesheet"
    href="{{ asset('vendor/fancybox/dist/jquery.fancybox.min.css') }}"
    />
    <link rel="stylesheet" href="{{ asset('css/novicons.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" />
  </head>
  <body>
    @include('components.header')

    @yield('content')

    <footer class="footer">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">
            <img
              src="{{ asset('assets/arpada-logo.svg') }}"
              alt="Logo Arpada"
              class="footer-arpada @if(Route::currentRouteName() == 'sales.module') d-none @endif"
            />
          </div>
          <div class="col-md-6">
            <div class="footer-center">
              <img
                src="{{ asset('assets/iso-small.svg') }}"
                alt="Isotipo Nowus"
                class="logo"
              />
              <div class="text-center">
                <p>
                  El vendedor de <span class="text-uppercase">Nowus</span> Centro Sur se reserva el derecho de
                  realizar modificaciones en los planos, diseños exteriores,
                  precios, materiales, especificaciones, acabados y
                  características estándar en cualquier momento y sin previo
                  aviso. Las fotografías, las representaciones son ilustrativas
                  y conceptuales.
                </p>
                <p>
                  <b
                    >Copyright © 2019 Arpada S.A. de C.V. | <a href="" class="color-black">Política de
                        Privacidad</a> | <a href="http://renderinc.mx" class="color-black" target="_blank">Desarrollo de sitios web Renderinc</a></b
                  >
                </p>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="footer-logos @if(Route::currentRouteName() == 'sales.module') d-none @endif">
              <img src="{{ asset('assets/logo-basico.svg') }}" alt="Logo Básico" />
              <img src="{{ asset('assets/logo-renderinc.svg') }}" alt="Logo Renderinc" />
              <img src="{{ asset('assets/bbr-logo.svg') }}" alt="Logo BBR" />
              <img src="{{ asset('assets/alvana.svg') }}" alt="Logo Alvana">
            </div>
          </div>
        </div>
      </div>
    </footer>

    <div class="fake-loader">
      <div>
        <img src="{{ asset('assets/iso.svg') }}" alt="" class="logo" />
        <img src="{{ asset('vendor/SVG-Loaders/svg-loaders/tail-spin.svg') }}" />
      </div>
    </div>

    @include('components.sticky-buttons')

    @if (Request::is('disponibilidad'))
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    @else
      <script src="{{ asset('vendor/jquery/dist/jquery.min.js') }}"></script>  
    @endif
    <script
      src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
      integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
      crossorigin="anonymous"
    ></script>
    <script src="//cdn.jsdelivr.net/gh/cferdinandi/smooth-scroll@14/dist/smooth-scroll.polyfills.min.js"></script>
    <script src="{{ asset('plugins/bootstrap-4.3.1-dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('vendor/wow/dist/wow.min.js') }}"></script>
    <script src="{{ asset('vendor/fancybox/dist/jquery.fancybox.min.js') }}"></script>
    <script src="{{ asset('vendor/sweetalert/dist/sweetalert.min.js') }}"></script>
    <script src="{{ asset('js/ajax.js') }}"></script>

    @yield('js-include')

    <script>
      var scroll = new SmoothScroll('.scroll', {
        header: '[data-scroll-header]',
        speed: 1000,
        speedDuration: true,
        easing: 'easeInOutCubic'
      });
	
      new WOW().init();
      // on cover image load
      // var coverImage = new Image();
      // coverImage.onload = function() {
      //   $(".fake-loader").fadeOut();
      // };
      // coverImage.src = "./assets/front-background.png";

      $(window).on("load", function() {
        $(".fake-loader").fadeOut();
        $("body").addClass("show-up");
      });

      $(document).ready(function() {
        $(".header-toggle").on("click", function() {
          $("body").toggleClass("is-open");
        });
        
        @stack('js-in-ready')
      }).on('scrollStop', function () {
        $('body').removeClass('is-open');
      });
    </script>
  </body>
</html>

