<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>{{ env('APP_NAME')}}</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/app.css">
    @yield('styles')
</head>
<body>
    <header>
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light">
                <div class="container-fluid">
                    <a class="navbar-brand" href="{{ route('home') }}"><img src="images/lg-laslomas.svg" alt="" class="logo" /></a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                        <ul class="navbar-nav mb-2 mb-lg-0">
                            <li class="nav-item"><a class="nav-link" href="{{ Request::route()->getName() === 'home' ? '#' : route('home') }}">HOME</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ Request::route()->getName() === 'home' ? '' : route('home') }}#nosotros">NOSOTROS</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ Request::route()->getName() === 'home' ? '' : route('home') }}#master-plan">MASTER PLAN</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ Request::route()->getName() === 'home' ? '' : route('home') }}#socio-club"><b>SOCIO CLUB</b></a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ Request::route()->getName() === 'home' ? '' : route('home') }}#contacto">CONTACTO</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </header>
    @yield('content')
    <footer class="{{ isset($isSalesModule) && $isSalesModule ? 'fixed' : ''}}">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <i>&copy; <span class="display-year"></span> Renderinc - Todos los derechos reservados. <a href="#" class="privacy-link privacy-link--desktop">Política de privacidad</a></i>
                </div>
                <div class="col-md-4 text-md-end">
                    <a class="navbar-brand" href="#"><img src="images/lg-laslomas-white.svg" alt="" class="logo" /></a>
                </div>
            </div>
        </div>
    </footer>

    <a href="https://api.whatsapp.com/send?phone=526681134546&text=%C2%A1Hola!%20Me%20gustar%C3%ADa%20recibir%20m%C3%A1s%20informaci%C3%B3n%20de%20Las%20Lomas" target="_blank" class="fixed-button fixed-button--whatsapp" rel="noreferrer" aria-label="Enviar mensaje por whatsapp"><img src="{{ asset('images/ico-whatsapp.svg') }}" alt="Whatsapp"></a>

    @yield('scripts')

    <script src="js/bootstrap.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script>
        var inputs = document.querySelectorAll('.form input, .form textarea')
        inputs.forEach(input => {
            input.addEventListener('focus',function(){
                this.closest('.form-group').classList.add('active');
            });
            input.addEventListener('focusout',function(){
                if(!this.value.length)
                    this.closest('.form-group').classList.remove('active');
            });
        });
    </script>
    <script>
        document.addEventListener("DOMContentLoaded", function(){
            window.addEventListener('click', function(e) {
                if(e.target.matches('.nav-link, .nav-link *')){
                    scrollToSection(e);
                }
            });
        }); 
        function scrollToSection(e){
            if(window.innerWidth < 768)
                document.querySelector('.navbar-toggler').click();
            const target = e.target?.getAttribute('href') ?? e.target.closest('.nav-link')?.getAttribute('href');
            if(!target || target.length <= 1 || target.charAt(0) !== "#") return;

            e.preventDefault();
            e.stopPropagation();
            const yOffset = (target === '#socio-club' || target === '#contacto') ? -80 : -140; 
            const element = document.querySelector(target);
            const y = element.getBoundingClientRect().top + window.pageYOffset + yOffset;
            window.scrollTo({top: y, behavior: 'smooth'});

            return false;
        }
    </script>
    <script src="js/contact.js"></script>
</body>
</html>